<?php 
	function has_permission_menu($nama_menu)
	{
		$ci = & get_instance();
		$ci->load->library('session');
		$query = $ci->db->query("SELECT user_role_level_2.id_user_role, role.id_role, permission_web.id_permission_web, menu.id_menu FROM user_role_level_2 INNER JOIN role ON user_role_level_2.id_role=role.id_role INNER JOIN permission_web ON role.id_role=permission_web.id_role INNER JOIN menu ON menu.id_menu=permission_web.id_menu WHERE user_role_level_2.id_user_role = '".$ci->session->userdata()['id_user_role_lv_2']."' AND menu.nama_menu='".$nama_menu."'");
		return $query->num_rows();
	}

	function has_category_menu($category_menu)
  {
    $ci = & get_instance();
    $ci->load->library('session');
    $query = $ci->db->query("SELECT user_role_level_2.id_user_role, role.id_role, permission_web.id_permission_web, menu.id_menu FROM user_role_level_2 INNER JOIN role ON user_role_level_2.id_role=role.id_role INNER JOIN permission_web ON role.id_role=permission_web.id_role INNER JOIN menu ON menu.id_menu=permission_web.id_menu WHERE user_role_level_2.id_user_role = '".$ci->session->userdata()['id_user_role_lv_2']."' AND menu.category='".$category_menu."'");
    return $query->num_rows();
  }

	function role_user()
	{
		$ci = & get_instance();
		$ci->load->library('session');
		$query = $ci->db->query("SELECT user_role_level_2.id_user_role, role.id_role, role.nm_role FROM user_role_level_2 INNER JOIN role ON user_role_level_2.id_role=role.id_role WHERE user_role_level_2.id_user_role = '".$ci->session->userdata()['id_user_role_lv_2']);
		return $query->row()->nm_role;
	}

 ?>