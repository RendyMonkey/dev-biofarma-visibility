<?php
    function search_datatable($name_table, $search_attribut, $column_order)
    {
        $ci = & get_instance();
        $ci->db->from($name_table);

		$i = 0;

		foreach($search_attribut as $search) {
			if($_POST['search']['value'])
			{
				if($i===0) // first loop
                {
                    $ci->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $ci->db->like($search, $_POST['search']['value']);
                }
                else
                {
                    $ci->db->or_like($search, $_POST['search']['value']);
                }
 
                if(count($search_attribut) - 1 == $i) //last loop
                    $ci->db->group_end(); //close bracket
			}
			
            $i++;
		}

		if(isset($_POST['order'])) // here order processing
        {
            $ci->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($ci->order))
        {
            $order = $ci->order;
            $ci->db->order_by(key($order), $order[key($order)]);
		}
    }
?>