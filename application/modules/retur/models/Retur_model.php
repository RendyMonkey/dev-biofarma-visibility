<?php 
class Retur_model extends CI_Model{

	public function c_retur($data){
		$query = $this->db->insert('retur', $data);
		return $query;
	}

	public function r_detail_retur_query($id){
		$search_attribut = ["dtl_retur.gsOnid", "dtl_retur.description_retur", "dtl_retur.UOM", "user_account.username"];
		$column_order = array(null, "dtl_retur.gsOnid", "dtl_retur.description_retur", "dtl_retur.UOM", "user_account.username");
		$this->db->select("dtl_retur.gsOnid, dtl_retur.description_retur, dtl_retur.UOM, user_account.username");
		$this->db->from('dtl_retur');
		$this->db->join('user_account', 'user_account.id_user_account = dtl_retur.id_user_account_scan_retur_from');
		$this->db->where('dtl_retur.no_retur', $id);
		$i = 0;	
	}

	public function r_retur_query(){
		$search_attribut = ["retur.*", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "no_retur", "no_do", "id_role_retur_to");
		$this->db->select('retur.*, user_role_level_2.nm_role_lvl_2');
		$this->db->from('retur');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = retur.id_role_retur_to');
		$i = 0;	
	}

	public function r_detail_retur($id){
		$this->r_detail_retur_query($id);
		if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_retur(){
		$this->r_retur_query();
		if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_retur_count_filtered()
    {
        $this->r_retur_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 	
 	function r_detail_retur_count_filtered($id)
    {
        $this->r_detail_retur_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function r_retur_count_all()
    {
        $this->db->from('retur');
        return $this->db->count_all_results();
    }

    public function r_detail_retur_count_all($id)
    {
        $this->db->from('dtl_retur');
        $this->db->where('no_retur', $id);
        return $this->db->count_all_results();
    }

	public function r_retur_with_detail_by_id($id){
		return $this->db->query("SELECT retur.*, dtl_retur.* FROM retur JOIN dtl_retur ON retur.no_retur=dtl_retur.no_retur WHERE retur.no_retur=".$id)->row();
	}

	public function u_retur($id, $data){
		$this->db->where('no_retur', $id);
		$query = $this->db->update('retur', $data);
		return $query;
	}

	public function d_retur($id){
		$query = $this->db->delete('retur', array('no_retur' => $id));
		return $query;
	}
	
	public function c_detail_retur($data){
		$query = $this->db->insert('datail_retur', $data);
		return $query;
	}


	public function r_detail_retur_by_id($id)
	{
		return $this->db->get_where('dtl_retur', ['no_retur' => $id])->result();
	}

	public function r_detail_retur_by_gsOneId($id)
	{
		return $this->db->get_where('datail_retur', ['gs_on_id' => $id])->row();
	}

	public function u_detail_retur($id, $data){
		$this->db->where('no_retur', $id);
		$query = $this->db->update('dtl_retur', $data);
		return $query;
	}

	public function d_detail_retur($id){
		$query = $this->db->delete('dtl_retur', array('no_retur' => $id));
		return $query;
	}
}
?>