<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class DetailRetur extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('DetailRetur_model');
 	}

 	public function createDetailRetur()
 	{
 		$data = [
 			"no_retur" => $this->input->post('no_retur'),
 			"gsOnid" => $this->input->post('gsOnid'),
 			"description_retur" => $this->input->post('description_retur'),
 			"UOM" => $this->input->post('UOM'),
 			"id_user_account_scan_retur_from" => $this->input->post('id_user_account_scan_retur_from')
 		];
 		$input = $this->DetailRetur_model->c_detail_retur($data);
 		if($input){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getDetailRetur()
 	{
 		$data = $this->DetailRetur_model->r_detail_retur();
 		echo json_encode($data);
 	}

 	public function updateDetailRetur($id)
 	{
 		$data = [
 			"gsOnid" => $this->input->post('gsOnid'),
 			"description_retur" => $this->input->post('description_retur'),
 			"UOM" => $this->input->post('UOM'),
 			"id_user_account_scan_retur_from" => $this->input->post('id_user_account_scan_retur_from')
 		];

 		$update = $this->DetailRetur_model->u_detail_retur($id, $data);
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteDetailRetur($id)
 	{
 		$delete = $this->DetailRetur_model->d_detail_retur($id);
 		echo json_encode(['message' => 'delete data berhasil']);
 	}
}