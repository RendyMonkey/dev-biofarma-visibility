<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Home extends CI_Controller {
	private $nama = "";

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('user/UserAccount_model'); 
 		$this->nama = $this->session->userdata()['nama'];
 		if(!$this->session->has_userdata('nama')){
 			redirect(base_url("login"));
 		}
 	}

 	public function index()
 	{
		$count_actived_user = $this->UserAccount_model->r_user_account_active();
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('home_page', ['actived_user_count' => $count_actived_user]);
		$this->load->view('_rootComponents/_footer/footer');
 	}
}