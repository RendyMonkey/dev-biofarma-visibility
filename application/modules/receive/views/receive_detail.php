<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Receive Order</a></li>
                                <li class="breadcrumb-item active">Detail</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Gudang Vaksin Diskes Prov Banten</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-primary">Receive Order Detail PRT-98237456</h5>

                            <div class="row">
                                <div class="col-6">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Product</th>
                                                <td>: Biocovac</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Receive Order</th>
                                                <td>: PRT-98237456</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Delivery Order</th>
                                                <td>: DO-5f9078FBCB4FC</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Tanggal Pengiriman</th>
                                                <td>: 2020-10-21</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Tanggal Penerimaan</th>
                                                <td>: 2020-10-21</td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Status</th>
                                                <td>: Diterima</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6">
                                <video id="webcam-preview" class="col"></video>
                                <center><button class="btn btn-primary self-align-center col-6" style="position: absolute;top: 40%;left: 27%;" id="scanner" onclick="startScan()">Start Scan</button></center>
                                </div>
                            </div>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>No Do</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                        <th>IMEI User Delivery</th>
                                        <th>IMEI User Received</th>
                                        <th>Status Receive</th>
                                        <th>Product Model</th>
                                        <th>Product Name</th>
                                        <th>gs one inner box</th>
                                        <th>gs one master box</th>
                                        <th>Qty inner box</th>
                                        <th>Qty Vial</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <button type="button" id="submit" class="btn btn-primary">Submit</button>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->

        <div class="modal fade" id="modal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">018994957000459910TES212172110212141A38510CC24, MasterBox Product BIOCOVAC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <table id="datatable1" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <tr class="text-left">
                                        <th>#</th>
                                        <th>No Do</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                        <th>IMEI User Delivery</th>
                                        <th>IMEI User Received</th>
                                        <th>Status Receive</th>
                                        <th>Product Model</th>
                                        <th>Product Name</th>
                                        <th>gs one inner box</th>
                                        <th>gs one master box</th>
                                        <th>Qty inner box</th>
                                        <th>Qty Vial</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>018994957000459910TES212172110212141A38510CC24</td>
                                        <td>MasterBox</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>01899495700076710TES2120172110212112B6560CEBDA</td>
                                        <td>MasterBox</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>01899495700076710TES21201721102121AD95C741C31A</td>
                                        <td>MasterBox</td>
                                    </tr>

                                    <tr>
                                        <td>4</td>
                                        <td>01899495700076710TES2120172110212166C62A082D6E</td>
                                        <td>MasterBox</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>01899495700076710TES21201721102121DF644A4843E9</td>
                                        <td>MasterBox</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">018994957000459910TES21201721102121ADA38510SC55, MasterBox Product BIOCOVAC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <table id="datatable2" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>01899495700076710TES212017211021211D9746F3BE90</td>
                                        <td>MasterBox</td>
                                    </tr>

                                    <tr>
                                        <td>2</td>
                                        <td>01899495700076710TES21201721102121D48A1056E77F</td>
                                        <td>MasterBox</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>01899495700076710TES21201721102121ACA06038F4E1</td>
                                        <td>MasterBox</td>
                                    </tr>

                                    <tr>
                                        <td>4</td>
                                        <td>01899495700076710TES212017211021214C73CFFE54C1</td>
                                        <td>MasterBox</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>01899495700076710TES2120172110212141A385102B24</td>
                                        <td>MasterBox</td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
        <!--end footer-->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    var table = $('#datatable').DataTable({
    'scrollX': true,
});

var details = [];
var objectDetail = ['<input class="form-control" type="text" name="">', '<input class="form-control" type="text" name="">', '<input class="form-control" type="text" name="">', '<input class="form-control" type="text" name="">', '<input class="form-control" type="text" name="">', '<input class="form-control" type="text" name="">'];
let count = 0;
    
    function startScan() {
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.decodeFromVideoDevice(null, 'webcam-preview', (result, err) => {
        if (result) {
            document.querySelector(".load").classList.remove("hidden");
            document.querySelector(".page-wrapper").classList.add("myStyle");
            $.ajax({
                url: '<?= base_url('delivery_order/DeliveryOrder/getDetailDeliveryOrderByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    if(data == null){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Item not found !',
                        });
                    }else{
                        count += 1;
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            html: '<p class="h4">Your data with GSOneID '+ result + ' is successfully scanned.</p>'
                        });
                        table.row.add([count,'<input class="form-control" type="text" name="no_do[]" value="'+data.no_do+'">', '<input class="form-control" type="text" name="gsOneId[]" value="'+data.gs_on_id+'">',  '<input class="form-control" type="text" name="uom[]" value="'+data.uom+'">', '<input class="form-control" type="text" name="imei_user_account_delivery[]" value="'+data.imei_user_account_delivery+'">', '<input class="form-control" type="text" name="imei_user_account_receive[]" value="'+data.imei_user_account_receive+'">', '<input class="form-control" type="text" name="status_received[]" value="'+data.status_received+'">', '<input class="form-control" type="text" name="product_model_id[]" value="'+data.product_model_id+'">', '<input class="form-control" type="text" name="product_name[]" value="'+data.product_name+'">', '<input class="form-control" type="text" name="gs_one_inner_box[]" value="'+data.gs_one_inner_box+'">', '<input class="form-control" type="text" name="gs_one_master_box[]" value="'+data.gs_one_master_box+'">', '<input class="form-control" type="text" name="qty_inner_box[]" value="'+data.qty_inner_box+'">', '<input class="form-control" type="text" name="qty_vial[]" value="'+data.qty_vial+'">']).draw();
                    }
                }
            });
            // properly decoded qr code
            // console.log('Found QR code!', result)
        codeReader.reset();
        $('#scanner').show();
    }
});

$('#scanner').hide();
}
    $('#datatable').DataTable();
    $('#datatable1').DataTable();
    $('#datatable2').DataTable();
    //    $('#datatable').DataTable({ 

    //     "processing": true, //Feature control the processing indicator.
    //     "serverSide": true, //Feature control DataTables' server-side processing mode.
    //     "order": [], //Initial no order.

    //     // Load data for the table's content from an Ajax source
    //     "ajax": {
    //       "url": "<?= site_url('retur/getRetur')?>",
    //       "type": "POST"
    //     },

    //     //Set column definition initialisation properties.
    //     "columnDefs": [
    //       { 
    //         "targets": [ 0 ], //first column / numbering column
    //         "orderable": false, //set not orderable
    //       },
    //     ],

    //   });
</script>