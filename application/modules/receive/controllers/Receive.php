<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Receive extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 	}

	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive_detail');
	   $this->load->view('_rootComponents/_footer/footer');
	}
}