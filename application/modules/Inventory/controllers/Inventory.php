<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Inventory extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 	}

	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('inventory');
	   $this->load->view('_rootComponents/_footer/footer');
	}
}
?>