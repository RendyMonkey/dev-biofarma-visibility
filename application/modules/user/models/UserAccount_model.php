<?php 
class UserAccount_model extends CI_Model{

	public function c_user_account($data){
		$query = $this->db->insert('user_account', $data);
		return $query;
	}

	public function r_user_account_query(){		
		$search_attribut = ["imei", "username", "nama_lengkap", "no_telephone", "user_account_email"];
		$column_order = array(null, "imei", "username", "nama_lengkap", "no_telephone", "user_account_email");
		search_datatable("user_account", $search_attribut, $column_order);
	}

	public function r_user_account(){
		$this->r_user_account_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	} 

	public function u_user_account($id, $data){
		$this->db->where('id_user_account', $id);
		$query = $this->db->update('user_account', $data);
		return $query;
	}

	public function d_user_account($id){
		$query = $this->db->delete('user_account', array('id_user_account' => $id));
		return $query;
	}

	public function r_imei_account_by_id($id)
	{
		return $this->db->get_where('user_account', ['id_user_account' => $id])->row();
	}

	public function r_user_account_by_role_level_2_id($id){
		return $this->db->get_where('user_account', ['id_role_level_2' => $id	])->result();
	}

	public function r_user_account_by_id($id){
		return $this->db->get_where('user_account', ['id_user_account' => $id])->row();
	}

	public function r_user_account_count_all()
    {
        $this->db->from('user_account');
        return $this->db->count_all_results();
	}
	
	function r_user_account_count_filtered()
    {
        $this->r_user_account_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function r_user_account_active()
    {
        return $this->db->get_where('user_account', ['is_active' => true])->num_rows();   
	}

	public function r_user_account_without_datatables(){
		return $this->db->get('user_account')->result();
	}
}
?>