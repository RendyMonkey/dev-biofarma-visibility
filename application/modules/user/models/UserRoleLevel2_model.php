<?php 
class UserRoleLevel2_model extends CI_Model{

	public function c_user_role_level_2($data){
		$query = $this->db->insert('user_role_level_2', $data);
		return $query;
	}

	public function r_user_role_level_2_query(){
		$search_attribut = ["kd_role_lvl_2", "nm_role_lvl_2", "alamat_lengkap", "telepon", "email", "fax", "location_werehouse"];
		$column_order = array(null, "kd_role_lvl_2", "nm_role_lvl_2", "alamat_lengkap", "telepon", "email", "fax", "location_werehouse");
		search_datatable("user_role_level_2", $search_attribut, $column_order);
	}

	public function r_role_without_datatables(){
		return $this->db->get('user_role_level_2')->result();
	}

	public function r_user_role_level_2() 
	{
		$this->r_user_role_level_2_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_user_role_level_2_count_filtered()
    {
        $this->r_user_role_level_2_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function r_user_role_level_2_count_all()
    {
        $this->db->from('user_role_level_2');
        return $this->db->count_all_results();
	}

	public function r_user_role_level_2_by_id($id){
		return $this->db->query("SELECT a.*, b.nm_role_lvl_2 as nm_role_parent, b.id_user_role as id_role_parent, c.nm_role FROM user_role_level_2 b INNER JOIN user_role_level_2 a ON b.id_user_role = a.id_role_parent INNER JOIN role c ON c.id_role = a.id_role WHERE a.id_user_role =".$id)->row();
	}

	public function u_user_role_level_2($id, $data){
		$this->db->where('id_user_role', $id);
		$query = $this->db->update('user_role_level_2', $data);
		return $query;
	}

	public function d_user_role_level_2($id){
		$query = $this->db->delete('user_role_level_2', array('id_user_role' => $id));
		return $query;
	}

	public function r_role_lvl_2_for_where_not_byId($id){
		$this->db->from('user_role_level_2');
		return $this->db->where_not_in('id_user_role', $id)->get()->result();
	}
}
?>