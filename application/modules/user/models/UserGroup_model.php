<?php 
class UserGroup_model extends CI_Model{

	public function c_user_group($data){
		$query = $this->db->insert('user_group', $data);
		return $query;
	}

	public function r_user_group(){
		$query = $this->db->get('user_group');
		return $query->result();
	}

	public function u_user_group($id, $data){
		$this->db->where('id_role_parent', $id);
		$query = $this->db->update('user_group', $data);
		return $query;
	}

	public function d_user_group($id){
		$query = $this->db->delete('user_group', array('id_role_parent' => $id));
		return $query;
	}
}
?>