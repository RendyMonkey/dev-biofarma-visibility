<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class UserRoleLevel2 extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('UserRoleLevel2_model');
 	}

 	public function createUserRoleLevel2()
 	{
 		$data = [
 			"id_role" => $this->input->post('id_role'),
 			"kd_role_lvl_2" => $this->input->post('kd_role_lvl_2'),
 			"nm_role_lvl_2" => $this->input->post('nm_role_lvl_2'),
 			"alamat_lengkap" => $this->input->post('alamat_lengkap'),
 			"telepon" => $this->input->post('telepon'),
 			"fax" => $this->input->post('fax'),
 			"email" => $this->input->post('email'),
			"location_warehouse" => $this->input->post('location_warehouse'),
			"nama_instansi" => $this->input->post('nama_instansi'),
			"contact_name" => $this->input->post('contact_name'),
			"id_role_parent" => $this->input->post('id_role_parent'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s")
		 ];
 		$input = $this->UserRoleLevel2_model->c_user_role_level_2($this->security->xss_clean($data));
 		if($input){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getUserRoleLevel2()
 	{
		$list = $this->UserRoleLevel2_model->r_user_role_level_2();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->kd_role_lvl_2;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->alamat_lengkap;
			$row[] = $data->telepon;
			$row[] = $data->fax;
			$row[] = $data->email;
			$row[] = $data->location_warehouse;
			$row[] = '<a href="'. base_url("role/update_lvl_2/").$data->id_user_role.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a href="#" onclick="delete_data('.$data->id_user_role.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->UserRoleLevel2_model->r_user_role_level_2_count_all(),
                        "recordsFiltered" => $this->UserRoleLevel2_model->r_user_role_level_2_count_filtered(),
                        "data" => $all_data,
		); 
		 echo json_encode($result);
		 
 		// $data = $this->UserRoleLevel2_model->r_user_user_role_level_2_level_2();
 		// echo json_encode($data);
 	}

 	public function updateUserRoleLevel2($id)
 	{
 		$data = [
			"id_role" => $this->input->post('id_role'),
			"kd_role_lvl_2" => $this->input->post('kd_role_lvl_2'),
			"nm_role_lvl_2" => $this->input->post('nm_role_lvl_2'),
			"alamat_lengkap" => $this->input->post('alamat_lengkap'),
			"telepon" => $this->input->post('telepon'),
			"fax" => $this->input->post('fax'),
			"email" => $this->input->post('email'),
		   "location_warehouse" => $this->input->post('location_warehouse'),
		   "nama_instansi" => $this->input->post('nama_instansi'),
		   "contact_name" => $this->input->post('contact_name'),
		   "id_role_parent" => $this->input->post('id_role_parent'),
		   "updated_by" => $this->session->userdata('id_user_account'),
		   "updated_date" => date("Y-m-d H:i:s")
 		];
 		$update = $this->UserRoleLevel2_model->u_user_role_level_2($id, $this->security->xss_clean($data));
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
	 }
	 
	 public function getUserlvl2ById($id) {
		 $data = $this->UserRoleLevel2_model->r_user_role_level_2_by_id($id);
		 echo json_encode($data);
	 }

	 public function getUserRoleLevel2AllWithoutDatatables(){
		echo json_encode($this->UserRoleLevel2_model->r_role_without_datatables());
	}

 	public function deleteUserRoleLevel2($id)
 	{
 		$delete = $this->UserRoleLevel2_model->d_user_role_level_2($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	 }
	 
	 public function getAllUserRoleLevel2WhereNotById($id){
		echo json_encode($this->UserRoleLevel2_model->r_role_lvl_2_for_where_not_byId($id));
	}
}