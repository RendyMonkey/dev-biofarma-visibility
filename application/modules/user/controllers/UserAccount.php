<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class UserAccount extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('UserAccount_model');
 	}

 	public function createUserAccount()
 	{
 		$data = [
			"id_role_level_2" => $this->input->post('id_user_role_level_2'),
 			"imei" => $this->input->post('imei'),
 			"username" => uniqid(10),
 			"password" => uniqid(10),
 			"nm_lengkap" => $this->input->post('nm_lengkap'),
 			"user_account_email" => $this->input->post('user_account_email'),
 			"no_telephone" => $this->input->post('no_telphone'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
			"activation_code" => rand()
			 
 		];
 		$input = $this->UserAccount_model->c_user_account($this->security->xss_clean($data));
 		if($input){
			 $data = [
				'email_penerima' => $data['user_account_email'],
				'subject' => 'Generate Account from Biofarma Admin',
				'body' => '<h3>Data Account</h3> 
				<p>Username = '.$data["username"]. '</p>
				<p>Password = '.$data['password'].'</p>
				<p>Activation Code = '.$data['activation_code'].'</p>'
			];
			echo json_encode(['message' => 'input data berhasil']);
			$this->load->library('mailer');
			$send = $this->mailer->send($data);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getUserAccount()
 	{
		$list = $this->UserAccount_model->r_user_account();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->imei;
			$row[] = $data->username;
			$row[] = $data->nm_lengkap;
			$row[] = $data->no_telephone;
			$row[] = $data->user_account_email;
			$row[] = '<a href="'. base_url("user/UserAccount/update/").$data->id_user_account .'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a>
                    <a href="#" onclick="delete_data('.$data->id_user_account.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->UserAccount_model->r_user_account_count_all(),
                        "recordsFiltered" => $this->UserAccount_model->r_user_account_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
	 }
	 
	 public function getUserAccountByRoleLevel2Id($id){
		echo json_encode($this->UserAccount_model->r_user_account_by_role_level_2_id($id));
	 }

	 public function getUserAccountWithoutDatatables(){
		 echo json_encode($this->UserAccount_model->r_user_account_without_datatables());
	 }

	 public function getUserAccountById($id){
		echo json_encode($this->UserAccount_model->r_user_account_by_id($id));
	 }

 	public function updateUserAccount($id)
 	{
 		$data = [
			"id_role_level_2" => $this->input->post('id_role_level_2'),
 			"imei" => $this->input->post('imei'),
 			"username" => $this->input->post('username'),
 			"nm_lengkap" => $this->input->post('nm_lengkap'),
 			"user_account_email" => $this->input->post('user_account_email'),
 			"no_telephone" => $this->input->post('no_telphone'),
 			"is_active" => $this->input->post('isActive'),
			"updated_by" => $this->session->userdata('id_user_account'),
		   	"updated_date" => date("Y-m-d H:i:s")
 		];
 		$update = $this->UserAccount_model->u_user_account($id, $this->security->xss_clean($data));
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteUserAccount($id)
 	{
 		$delete = $this->UserAccount_model->d_user_account($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}
	 
	 
	//  FOR VIEW
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/list_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/detail_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/create_user_account');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('userAccount/update_user_account', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}