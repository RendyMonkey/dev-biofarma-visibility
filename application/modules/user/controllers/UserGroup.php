<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class UserGroup extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('UserGroup_model');
 	}

 	public function createUserGroup()
 	{
 		$data = [
 			"id_role_parent" => $this->input->post('id_role_parent'),
 			"id_role_child" => $this->input->post('id_role_child')
 		];
 		$input = $this->UserGroup_model->c_user_group($data);
 		if($input){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getUserGroup()
 	{
 		$data = $this->UserGroup_model->r_user_group();
 		echo json_encode($data);
 	}

 	public function updateUserGroup($id)
 	{
 		$data = [
 			"id_role_parent" => $this->input->post('id_role_parent'),
 			"id_role_child" => $this->input->post('id_role_child')
 		];
 		$update = $this->UserGroup_model->u_user_group($id, $data);
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteUserGroup($id)
 	{
 		$delete = $this->UserGroup_model->d_user_group($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	 }
	 
	 
	//  FOR VIEW
	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('userGroup/list_user_group');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function detail()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('userGroup/detail_user_group');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('userGroup/create_user_group');
		$this->load->view('_rootComponents/_footer/footer');
	 }
}