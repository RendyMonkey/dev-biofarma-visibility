<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class recooling extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->helper('file');
 		$this->load->model('recooling_model');
 	}

 	public function getRecooling()
 	{
		$list = $this->recooling_model->r_recooling();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->category;
			$row[] = $data->description;
			$row[] = $data->foto;
			$row[] = $data->time_recooling;
			$row[] = $data->username;
			$row[] = '<a href='. base_url("recooling/listDoRecooling/").$data->id_recooling.' class="mr-2">
                       <i class="las la-clipboard font-18"></i></a>';
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->recooling_model->r_recooling_count_all(),
                        "recordsFiltered" => $this->recooling_model->r_recooling_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function getReportRecooling($id)
 	{
		$list = $this->recooling_model->r_report_recooling($id);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->eta;
			$row[] = $data->no_request;
			$row[] = $data->nm_role_lvl_2;
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->recooling_model->r_report_recooling_count_all($id),
                        "recordsFiltered" => $this->recooling_model->r_report_recooling_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function getRecoolingById($id){
		echo json_encode($this->recooling_model->r_recooling_by_id($id));
	}

	// FOR VIEW
	public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('list_recooling');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function listDoRecooling($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('list_do_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('detail_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function create()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('create_recooling');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('update_recooling', ['id' => $id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}