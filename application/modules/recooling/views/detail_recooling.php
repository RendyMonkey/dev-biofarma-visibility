<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Metrica</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                                <li class="breadcrumb-item active">List</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Detail recooling</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <img id="photo" src="" class="img-fluid">
                                </div>
                                    <div class="col-lg-6 align-self-center">
                                        <div class="single-pro-detail">
                                            <p class="mb-1">Detail recooling</p>
                                            <div class="custom-border"></div>
                                            <h3 class="pro-title" id="kategori_recooling"></h3>
                                            <p class="text-muted mb-4">Create By : <span id="user"></span></p>
                                           
                                           <label class="mb-2">Time recooling</label>
                                            <h6 class="pro-price mt-0" id="time_recooling"></h6>
                                            <h6 class="text-muted font-13">Description :</h6>
                                            <ul class="list-unstyled pro-features border-0" id="description">
                                            </ul>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- container -->
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

    <script>
    $.ajax({
        url: "<?= base_url('recooling/getrecoolingWithDetailById/').$id ?>",
        type: 'get',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $("#kategori_recooling").text(data.category_recooling);
            $("#user").text(data.id_user_account);
            $("#time_recooling").text(data.time_recooling);
            $("#description").text(data.descriptionrecooling);
            $("#photo").attr('src', '<?= base_url('assets/images/recooling/') ?>' + data.foto_recooling);

        }
    });
    </script>