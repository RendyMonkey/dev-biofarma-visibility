<?php 
class recooling_model extends CI_Model{

	public function r_recooling_query(){		
		$search_attribut = ["recooling.id_recooling", "recooling.category", "recooling.description", "recooling.foto", "recooling.time_recooling"," user_account.username"];
		$column_order = array(null, "recooling.id_recooling", "recooling.category", "recooling.description", "recooling.foto", "recooling.time_recooling"," user_account.username");
		$this->db->select("recooling.id_recooling, recooling.category, recooling.description, recooling.foto, recooling.time_recooling, user_account.username");
		$this->db->from('recooling');
		$this->db->join('user_account', 'user_account.id_user_account = recooling.id_user_account');
		$i = 0;	
	}

	public function r_report_recooling_query($id){
		$search_attribut = ["delivery_order.no_do", "delivery_order.eta", "delivery_order.no_request", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "delivery_order.no_do", "delivery_order.eta", "delivery_order.no_request", "user_role_level_2.nm_role_lvl_2");
		$this->db->select("delivery_order.no_do, delivery_order.eta, delivery_order.no_request, user_role_level_2.nm_role_lvl_2");
		$this->db->from('recooling');
		$this->db->join('recooling_do', 'recooling_do.id_recooling = recooling.id_recooling');
		$this->db->join('delivery_order', 'delivery_order.no_do = recooling_do.no_do');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('recooling.id_recooling', $id);
		$i = 0;
	}

	public function r_recooling(){
		$this->r_recooling_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_report_recooling($id){
		$this->r_report_recooling_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_recooling_count_all()
    {
        $this->db->from('recooling');
        return $this->db->count_all_results();
	}
	
	function r_recooling_count_filtered()
    {
        $this->r_recooling_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function r_report_recooling_count_all($id)
    {
        $this->db->from('recooling');
        $this->db->where('id_recooling', $id);
        return $this->db->count_all_results();
	}
	
	function r_report_recooling_count_filtered()
    {
        $this->r_report_recooling_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_recooling_by_id($id){
		$this->db->select("recooling.id_recooling, recooling.category, recooling.description, recooling.time_recooling, user_account.username");
		$this->db->from('recooling');
		$this->db->join('user_account', 'user_account.id_user_account = recooling.id_user_account');
		$query = $this->db->where('recooling.id_recooling', $id)->get()->row();
		return $query;
	}
}
?>