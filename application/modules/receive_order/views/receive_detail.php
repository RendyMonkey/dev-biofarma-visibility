<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<style type="text/css">
    .myStyle{
        filter: blur(3px);
  -webkit-filter: blur(3px);
    }
body{ 
    width:100%;
   height:100%;
   margin:0;
   background-color:#fff;

}

.load{
  z-index: 999;
  position:absolute;
  max-width:100px;
  margin:0 auto;
  top: 50%;
  left:50%;
  transform: translate(-50%, -50%);
}



/*loading screen*/



.loading-screen{
  float:left;
  height:20px;
  width: 20px;
  margin:0 5px;
  border-radius:50%;
  animation: shrink 1s ease infinite 0ms;
  transform: scale(0.35);
}



/* animation */


.loading-screen:nth-child(1){
  animation: shrink 1s ease infinite 350ms;
  background-color:#45aaf2;
}

.loading-screen:nth-child(2){
  animation: shrink 1s ease infinite 550ms;
  background-color:#ffb8b8;
}

.loading-screen:nth-child(3){
  animation: shrink 1s ease infinite 700ms; 
  background-color:#f9ca24;
}



@keyframes shrink{
  50%{
    -webkit-transform: scale(1);
            transform: scale(1);
        opacity: 1;
  }
  
100%{
  opacity: 0;
}


}

.hidden{
    display: none;
}
</style>
<div class="load hidden">
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
  <div class="loading-screen"></div>
</div>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Receive Order</a></li>
                                <li class="breadcrumb-item active">Detail</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Gudang Vaksin Diskes Prov Banten</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-primary">Receive Order Detail PRT-98237456</h5>

                            <div class="row">
                                <div class="col-6">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Delivery Order</th>
                                                <td id="no_do"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Supplier Name</th>
                                                <td id="supplier"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Tanggal Pengiriman</th>
                                                <td id="tanggal_pengiriman"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Tanggal Penerimaan</th>
                                                <td id="tanggal_penerimaan"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Status</th>
                                                <td id="status"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-6 col-xs-6">
                                <video id="webcam-preview" class="col"></video>
                                <center><button class="btn btn-primary self-align-center col-6" style="position: absolute;top: 40%;left: 27%;" id="scanner" onclick="startScan()">Start Scan</button></center>
                                </div>
                            </div>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>No Do</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                        <th>Status Receive</th>
                                        <th>Product Model</th>
                                        <th>Product Name</th>
                                        <th>gs one inner box</th>
                                        <th>gs one master box</th>
                                        <th>Qty inner box</th>
                                        <th>Qty Vial</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="button" id="submit" class="btn btn-primary">Submit</button>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->

        <div class="modal fade" id="modal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">018994957000459910TES212172110212141A38510CC24, MasterBox Product BIOCOVAC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <table id="datatable1" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <tr class="text-left">
                                        <th>#</th>
                                        <th>No Do</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                        <th>IMEI User Delivery</th>
                                        <th>IMEI User Received</th>
                                        <th>Status Receive</th>
                                        <th>Product Model</th>
                                        <th>Product Name</th>
                                        <th>gs one inner box</th>
                                        <th>gs one master box</th>
                                        <th>Qty inner box</th>
                                        <th>Qty Vial</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">018994957000459910TES21201721102121ADA38510SC55, MasterBox Product BIOCOVAC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <table id="datatable2" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>GsOneId</th>
                                        <th>UOM</th>
                                    </tr>
                                </thead>
                                <tbody>
                        
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
        <!--end footer-->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $.ajax({
        url: '<?= base_url('receive_order/ReceiveOrder/getReceiveOrderByNoDo/').$id;?>',
        type: 'get',
        dataType: 'json',
        success: function(data) {
            document.getElementById("no_do").innerHTML = ": " + data.no_do;
            document.getElementById("supplier").innerHTML = ": " + data.nm_role_lvl_2;
            document.getElementById("tanggal_pengiriman").innerHTML = ": " + data.tanggal_pengiriman;
            if(data.time_finish_receive == null){
                document.getElementById("tanggal_penerimaan").innerHTML = ":  -" ;
            }else{
                document.getElementById("tanggal_penerimaan").innerHTML = ": " + data.time_finish_receive;
            }
            if (data.status == 1) {
                document.getElementById("status").innerHTML = ": Dalam Perjalanan";
            }else if(data.status == 2){
                document.getElementById("status").innerHTML = ": Sedang dalam penerimaan";
            }else{
                document.getElementById("status").innerHTML = ": Sudah diterima";
            }
        }
    });

    var table = $('#datatable').DataTable({
        'scrollX': true,
    });

let arrayGsOneId = [];
let haveScanned = false;
let count = 0;    
    function startScan() {
    const codeReader = new ZXing.BrowserMultiFormatReader();
    codeReader.decodeFromVideoDevice(null, 'webcam-preview', (result, err) => {
        if (result) {
            var ketersediaan = false;
            $.ajax({
                url: '<?= base_url('delivery_order/DeliveryOrder/getDetailDeliveryOrderByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    if(data != null){
                        ketersediaan = true;
                    }
                }
            });

            var scanned = arrayGsOneId.find(function check(gsOneId) {
                 return gsOneId == result;
            });
            document.querySelector(".load").classList.remove("hidden");
            document.querySelector(".page-wrapper").classList.add("myStyle");
            $.ajax({
                url: '<?= base_url('delivery_order/DeliveryOrder/getDetailDeliveryOrderByGsOneId/') ?>' + result,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    document.querySelector(".load").classList.add("hidden");
                    document.querySelector(".page-wrapper").classList.remove("myStyle");
                    if(data == null){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Item not found !',
                        });
                    }else{
                        arrayGsOneId.push(data.gs_on_id);
                        count += 1;
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            html: '<p class="h4">Your data with GSOneID '+ result + ' is successfully scanned.</p>'
                        });
                        table.row.add([count,'<input class="form-control" type="text" name="no_do[]" value="'+data.no_do+'">', '<input class="form-control" type="text" name="gsOneId[]" value="'+data.gs_on_id+'">',  '<input class="form-control" type="text" name="uom[]" value="'+data.uom+'">', '<input class="form-control" type="text" name="status_received[]" value="'+data.status_received+'">', '<input class="form-control" type="text" name="product_model_id[]" value="'+data.product_model_id+'">', '<input class="form-control" type="text" name="product_name[]" value="'+data.product_name+'">', '<input class="form-control" type="text" name="gs_one_inner_box[]" value="'+data.gs_one_inner_box+'">', '<input class="form-control" type="text" name="gs_one_master_box[]" value="'+data.gs_one_master_box+'">', '<input class="form-control" type="text" name="qty_inner_box[]" value="'+data.qty_inner_box+'">', '<input class="form-control" type="text" name="qty_vial[]" value="'+data.qty_vial+'">']).draw();
                    }
                },
                error: function () {
                        document.querySelector(".load").classList.add("hidden");
                        document.querySelector(".page-wrapper").classList.remove("myStyle");
                        Swal.fire(
                            'Input Failed',
                            'Please contact developer to fix it.',
                            'error'
                        )
                    }
            });
            // properly decoded qr code
            // console.log('Found QR code!', result)
        codeReader.reset();
        $('#scanner').show();
    }
});

$('#scanner').hide();
}
    // $('#datatable').DataTable();
    // $('#datatable1').DataTable();
    // $('#datatable2').DataTable();
    $(document).ready(function () {
        var values = [];
        $("#submit").click(function () {
            for(var i = 0; i < document.getElementsByName("gsOneId[]").length; i++) {
                var object = {
                    no_do: document.getElementsByName("no_do[]")[i].value,
                    gsOneId : document.getElementsByName("gsOneId[]")[i].value,
                    uom : document.getElementsByName("uom[]")[i].value,
                    product_model_id : document.getElementsByName("product_model_id[]")[i].value,
                    product_name : document.getElementsByName("product_name[]")[i].value,
                    gs_one_inner_box : document.getElementsByName("gs_one_inner_box[]")[i].value,
                    gs_one_master_box : document.getElementsByName("gs_one_master_box[]")[i].value,
                    qty_inner_box : document.getElementsByName("qty_inner_box[]")[i].value,
                    qty_vial : document.getElementsByName("qty_vial[]")[i].value,
                    status_received : document.getElementsByName("status_received[]")[i].value,
                };
            values.push(object);
            }

            var data = {
                <?= $this->security->get_csrf_token_name(); ?> : '<?=$this->security->get_csrf_hash();?>',
                detail_order : values
            };

            $.ajax({
                url: '<?= base_url('delivery_order/DeliveryOrder/createDetailWithoutParent') ?>',
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        html: '<p class="h4">Your data is successfully input.</p>' +
                            '<a class="card-lin" href="<?= base_url('delivery_order/DeliveryOrder/list') ?>">Go to list page!</a>'
                    });

                    $("#date-format").val('');
                    $("#eta_hours").val('');
                    $("#quantity").val('');
                    $("#receiver").val('');
                    $("#no_do").val('');
                    $("#no_request").val('');
                    $("#parent_do").val('choose');
                    values = [];
                },
                error: function () {
                    values = [];
                    alert('gagal');
                }
            });
        });
    });
    
</script>