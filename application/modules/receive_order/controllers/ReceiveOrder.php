<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class ReceiveOrder extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('ReceiveOrder_model');
 	}

 	public function getReceiveOrder()
	 { 
		$list = $this->ReceiveOrder_model->r_receive_order();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->tanggal_pengiriman;
			$row[] = $data->time_finish_receive;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = '
					   <a href="'.base_url('receive_order/ReceiveOrder/detail/').$data->no_do.'" class="mr-2">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->ReceiveOrder_model->r_receive_order_count_all(),
                        "recordsFiltered" => $this->ReceiveOrder_model->r_receive_order_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }

	public function getReceiveOrderByNoDo($no_do){
		echo json_encode($this->ReceiveOrder_model->r_receive_order_by_no_do($no_do));
	}

    public function list()
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive');
	   $this->load->view('_rootComponents/_footer/footer');
	}

	public function detail($id)
	{
	   $this->load->view('_rootComponents/_header/heder');
	   $this->load->view('_rootComponents/_sidebar/sidebar');
	   $this->load->view('_rootComponents/_navbar/navbar');
	   $this->load->view('receive_detail', ['id'=>$id]);
	   $this->load->view('_rootComponents/_footer/footer');
	}
}