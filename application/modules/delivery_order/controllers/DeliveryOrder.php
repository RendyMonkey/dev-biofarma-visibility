<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class DeliveryOrder extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('DeliveryOrder_model');
 		$this->load->model('User/UserAccount_model');
 	}

 	public function createDeliveryWithDetailOrder()
 	{
		$no_do = rand();
		$no_request = rand();
 		$data = [
			"tanggal_pengiriman" => $this->input->post('tanggal_pengiriman'),
			"time_start_delivery" => date("Y-m-d H:i:s"),	
 			"status" => 1,
 			"no_request" => $this->input->post('no_request'),
 			"no_do" => $this->input->post('no_delivery'),
 			"id_role_delivery" => $this->session->userdata('id_user_role_lv_2'),
 			"id_role_receive" => $this->input->post('id_role_receive'),
 			"eta" => $this->input->post('ETA'),
			"qty_delivery" => $this->input->post('qty_delivery'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
			"parent_do" => $this->input->post('parent_do'),			   
			"uom" => $this->input->post('uom'),			   
 		];
 		$input_data = $this->DeliveryOrder_model->c_delivery_order($data);
 		if ($this->input->post('detail_order') != null) {
 			$this->createDetail($this->input->post('detail_order'));
 		}
 		if($input_data){
 			echo json_encode($this->input->post('detail_order'));
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function createDetail($detail_order){
 		foreach ($detail_order as $detail) {
 			$data = [
 				"no_do" => $detail['no_do'],
 				"gs_on_id" => $detail['gsOneId'],
 				"created_by" => $this->session->userdata('id_user_account'),
 				"created_date" => date("Y-m-d H:i:s"),
 				"product_model_id" => $detail['product_model_id'],
 				"product_name" => $detail['product_name'],
 				"gs_one_inner_box" => $detail['gs_one_inner_box'],
 				"gs_one_master_box" => $detail['gs_one_master_box'],
 				"qty_inner_box" => $detail['qty_inner_box'],
 				"qty_vial" => $detail['qty_vial'],
 			];
 			$input_data = $this->DeliveryOrder_model->c_detail_delivery_order($data);
 		}
 		if($input_data){
 			return true;
 		}else{
 			return false;
 		}
 	}

 	public function createDetailWithoutParent(){
 		foreach ($this->input->post('detail_order') as $detail) {
 			$data = [
 				"no_do" => $detail['no_do'],
 				"gs_on_id" => $detail['gsOneId'],
 				"created_by" => $this->session->userdata('id_user_account'),
 				"created_date" => date("Y-m-d H:i:s"),
 				"product_model_id" => $detail['product_model_id'],
 				"product_name" => $detail['product_name'],
 				"gs_one_inner_box" => $detail['gs_one_inner_box'],
 				"gs_one_master_box" => $detail['gs_one_master_box'],
 				"qty_inner_box" => $detail['qty_inner_box'],
 				"qty_vial" => $detail['qty_vial'],
 			];
 			$input_data = $this->DeliveryOrder_model->c_detail_delivery_order($data);
 		}
 		if($input_data){
 			echo json_encode($this->input->post('detail_order'));
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getDeliveryOrder()
	 { 
		$list = $this->DeliveryOrder_model->r_delivery_order();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->no_request;
			$row[] = $this->db->query('SELECT * FROM user_role_level_2 WHERE id_user_role ='.$data->id_role_delivery)->row()->nm_role_lvl_2;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->tanggal_pengiriman;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = $data->qty_delivery;
			$row[] = '<a href="'.base_url('delivery_order/DeliveryOrder/update/').$data->no_do.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a>
					   <a href="'.base_url('delivery_order/DeliveryOrder/detail/').$data->no_do.'" class="mr-2">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a> 
                    <a onclick="delete_data('.$data->no_do.')" href="#" class="delete" data-id="'.$data->no_do.'" >
					 <i class="las la-trash-alt text-danger font-18 del" data-id="'.$data->no_do.'"></i>
												</a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->DeliveryOrder_model->r_delivery_order_count_all(),
                        "recordsFiltered" => $this->DeliveryOrder_model->r_delivery_order_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }

	 public function getDetailDeliveryOrder()
	 { 
	 	$id = $this->input->post('id');
		$list = $this->DeliveryOrder_model->r_detail_delivery_order($id);
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->gs_on_id;
			$row[] = $data->uom;
			$row[] = $data->imei_user_account_delivery;
			$row[] = $data->imei_user_account_receive;
			$row[] = $data->username_delivery;
			$row[] = $data->username_receive;
			$row[] = $data->status_received;
			$row[] = $data->product_model_id;
			$row[] = $data->product_name;

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->DeliveryOrder_model->r_detail_delivery_order_count_all($id),
                        "recordsFiltered" => $this->DeliveryOrder_model->r_detail_delivery_order_count_filtered($id),
                        "data" => $datas,
		); 
 		echo json_encode($result);
	 }
	 
	 public function getDeliveryOrderStatusEmergency()
	 { 
		$list = $this->DeliveryOrder_model->r_delivery_order_status_emergency();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->no_request;
			$row[] = $this->db->query('SELECT * FROM user_role_level_2 WHERE id_user_role ='.$data->id_role_delivery)->row()->nm_role_lvl_2;
			$row[] = $data->nm_role_lvl_2;
			$row[] = $data->tanggal_pengiriman;
			if($data->status == 1){
				$row[] = "Dalam pengiriman";
			}else if($data->status == 2){
				$row[] = "Sedang dalam penerimaan";
			}else{
				$row[] = "Sudah diterima";
			}
			$row[] = $data->qty_delivery;
			$row[] = '<a href="'.base_url('delivery_order/DeliveryOrder/resend/').$data->no_do.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> ';
			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->DeliveryOrder_model->r_delivery_order_count_filtered_status_emergency(),
                        "recordsFiltered" => $this->DeliveryOrder_model->r_delivery_order_count_filtered_status_emergency(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
 	}

 	public function getDeliveryWithDetailOrderById($id)
 	{
 		echo json_encode($this->DeliveryOrder_model->r_delivery_order_with_detail_by_id($id));
 	}

 	public function getDetailDeliveryOrderById($id)
 	{
 		echo json_encode($this->DeliveryOrder_model->r_detail_delivery_order_by_id($id));
 	}

 	public function updateDeliveryWithDetailOrder($id)
 	{
		$time_finish_receive = null;
		if($this->input->post('status') == 3){
			$time_finish_receive = date("Y-m-d");	
		}
		$data = [
			"tanggal_pengiriman" => $this->input->post('tanggal_pengiriman'),
			"status" => $this->input->post('status'),
			"id_role_receive" => $this->input->post('id_role_receive'),
			"eta" => $this->input->post('ETA'),
			"qty_delivery" => $this->input->post('qty_delivery'),
			"updated_by" => $this->session->userdata('id_user_account'),
			"updated_date" => date("Y-m-d H:i:s"),
		];

 		$update_data = $this->DeliveryOrder_model->u_delivery_order($id, $data);
 		if($update_data){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
	 }
	 
	 public function resendDelivery($id)
 	{
		$data = [
			"status" => 1,
			"eta" => $this->input->post('ETA'),
			"updated_by" => $this->session->userdata('id_user_account'),
			"updated_date" => date("Y-m-d H:i:s"),
		];

 		$update_data = $this->DeliveryOrder_model->u_delivery_order($id, $data);
 		if($update_data){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
 	}

 	public function deleteDeliveryWithDetailOrder($id)
 	{
 		$delete = $this->DeliveryOrder_model->d_delivery_order($id);
 		$delete_detail = $this->DeliveryOrder_model->d_detail_delivery_order($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function getDeliveryOrderWithoutDatatable(){
		echo json_encode($this->DeliveryOrder_model->r_delivery_order_without_datatable());
	}

	public function getDetailDeliveryOrderByGsOneId($id){
		echo json_encode($this->DeliveryOrder_model->r_detail_delivery_order_by_gsOneId($id));
	}

	public function getTrackBackDeliveryByParentDo($no_do){
		$loop = true;
		$data = [];
		$search_by = $no_do;
		while ($loop) {
			$track = $this->DeliveryOrder_model->r_trackback_delivery_order($search_by);
		if ($track != null) {
			$search_by = $track->parent_do;
				array_push($data, $track);
			}else{
				$loop = false;
			}	
		}
		
		echo json_encode($data);
	}
	 
	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function list_pending()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_pending_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function detail($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('detail_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }
 
	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_delivery');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function resend($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('resend_delivery', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function trackBackByGsOne()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('track_back_by_GsOne');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function trackBackByNoDo()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('track_back_by_NoDo');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function getDeliveryOrderByIdJoinUserLevel2($id){
		echo json_encode($this->DeliveryOrder_model->r_delivery_order_join_user_rol_lvl_2_byId($id));
	 }

	 public function test(){
	 	echo json_encode($this->DeliveryOrder_model->r_trackback_delivery_order());
	 }
	 
}