<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
  type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>

<div class="page-wrapper">
  <!-- Page Content-->
  <div class="page-content-tab">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="float-right">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0);">Report</a></li>
                <li class="breadcrumb-item active">Trackback DO</li>
              </ol>
            </div>
            <h4 class="page-title">List Trackback DO</h4>
          </div>
          <!--end page-title-box-->
        </div>
        <!--end col-->
      </div><!-- end page title end breadcrumb -->

      <div class="row">
        <div class="col-3 mb-3">
         <select class="form-control theSelect" id="uom">
              <option value="">Choose...</option>
              <option value="1">MasterBox</option>
              <option value="2">InnerBox</option>
              <option value="3">Vial</option>
          </select>
        </div>
        <div class="col-3">
          <button type="button" id="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>

      <div class="row">
      <div class="col-12">
          <div class="card">
            <div class="card-body">
              <table id="datatable" class="table table-bordered nowrap"
                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Delivery Order NUmber</th>
                    <th>Ship To</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- container -->

    <!--  Modal content for the above example -->
    <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking <span
        class="text-muted d-none d-sm-inline-block float-right"></i>
        by Mannatthemes</span></footer>
    <!--end footer-->
  </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
$(".theSelect").select2();
var table = $('#datatable').DataTable({
              "scrollX": true,
              "processing": true,
              "serverSide": true,
              "order": [],

              "ajax": {
                "url": "<?= site_url('')?>",
                "type": "POST"
              },

              "columnDefs": [{
                "targets": [0],
                "orderable": false,
              }, ],

            });

</script>