<?php 
class DeliveryOrder_model extends CI_Model{

	public function c_delivery_order($data){
		$query = $this->db->insert('delivery_order', $data);
		return $query;
	}

	public function r_detail_delivery_order_query($id){
		$search_attribut = ["a.gs_on_id", "a.uom", "a.imei_user_account_delivery", "a.imei_user_account_receive", "b.username", "c.username", "a.status_received", "a.product_model_id", "a.product_name"];
		$column_order = array(null, "a.gs_on_id", "a.uom", "a.imei_user_account_delivery", "a.imei_user_account_receive", "b.username", "c.username", "a.status", "a.product_model_id", "a.product_name");
		$this->db->select("a.*, b.username as username_delivery, c.username as username_receive");
		$this->db->from('detail_delivery_order a');
		$this->db->join('user_account b', 'b.id_user_account = a.id_user_account_delivery');
		$this->db->join('user_account c', 'c.id_user_account = a.id_user_account_receive');
		$this->db->where('a.no_do', $id);
		$i = 0;
	}

	public function r_delivery_order_query(){
		$search_attribut = ["delivery_order.no_do", "delivery_order.no_request", "delivery_order.tanggal_pengiriman", "delivery_order.status", "delivery_order.qty_delivery", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "delivery_order.no_do", "delivery_order.no_request", "delivery_order.tanggal_pengiriman", "delivery_order.status", "delivery_order.qty_delivery", "user_role_level_2.nm_role_lvl_2");
		$this->db->select("delivery_order.no_do, delivery_order.no_request, delivery_order.id_role_delivery, delivery_order.id_role_receive, delivery_order.tanggal_pengiriman, delivery_order.status, delivery_order.qty_delivery, user_role_level_2.nm_role_lvl_2");
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$i = 0;
	}

	public function r_trackback_delivery_order($no_do){
		$this->db->select("delivery_order.no_do, delivery_order.parent_do, delivery_order.id_role_receive, delivery_order.tanggal_pengiriman, user_role_level_2.nm_role_lvl_2");
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('delivery_order.no_do', $no_do);
		return $this->db->get()->row();
	}

	public function r_delivery_order_query_status_emergency(){
		$search_attribut = ["delivery_order.no_do", "delivery_order.no_request", "delivery_order.tanggal_pengiriman", "delivery_order.status", "delivery_order.qty_delivery", "user_role_level_2.nm_role_lvl_2"];
		$column_order = array(null, "delivery_order.no_do", "delivery_order.no_request", "delivery_order.tanggal_pengiriman", "delivery_order.status", "delivery_order.qty_delivery", "user_role_level_2.nm_role_lvl_2");
		$this->db->select("delivery_order.no_do, delivery_order.no_request, delivery_order.id_role_delivery, delivery_order.id_role_receive, delivery_order.tanggal_pengiriman, delivery_order.status, delivery_order.qty_delivery, user_role_level_2.nm_role_lvl_2");
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		$this->db->where('delivery_order.is_emergency =', 2);
		$i = 0;
	}

	public function r_delivery_order() 
	{
		$this->r_delivery_order_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_detail_delivery_order($id) 
	{
		$this->r_detail_delivery_order_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	public function r_delivery_order_status_emergency() 
	{
		$this->r_delivery_order_query();
		$this->db->where('delivery_order.is_emergency =', 2);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
	}

	function r_delivery_order_count_filtered()
    {
        $this->r_delivery_order_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 	
 	function r_detail_delivery_order_count_filtered($id)
    {
        $this->r_detail_delivery_order_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

	function r_delivery_order_count_filtered_status_emergency()
    {
        $this->r_delivery_order_query_status_emergency();
        $query = $this->db->get();
        return $query->num_rows();
	}
	
    public function r_delivery_order_count_all()
    {
        $this->db->from('delivery_order');
        return $this->db->count_all_results();
	}

	public function r_detail_delivery_order_count_all($id)
    {
        $this->db->from('detail_delivery_order');
        $this->db->where('no_do', $id);
        return $this->db->count_all_results();
	}
	
	public function r_delivery_order_count_all_status_emergency()
    {
        $this->db->from('delivery_order');
        $this->db->where('is_emrgency', 2);
        return $this->db->count_all_results();
    }

	public function r_delivery_order_with_detail_by_id($id)
	{
		return $this->db->query("SELECT delivery_order.*,detail_delivery_order.* FROM delivery_order JOIN detail_delivery_order ON delivery_order.no_do=detail_delivery_order.no_do WHERE delivery_order.no_do=".$id)->row();
	}

	public function u_delivery_order($id, $data){
		$this->db->where('no_do', $id);
		$query = $this->db->update('delivery_order', $data);
		return $query;
	}

	public function d_delivery_order($id){
		$query = $this->db->delete('delivery_order', array('no_do' => $id));
		return $query;
	}

	public function c_detail_delivery_order($data){
		$query = $this->db->insert('detail_delivery_order', $data);
		return $query;
	}

	// public function r_detail_delivery_order(){
	// 	$query = $this->db->get('detail_delivery_order');
	// 	return $query->result();
	// }

	public function r_detail_delivery_order_by_id($id){
		return $this->db->get_where('detail_delivery_order', ['no_do' => $id])->result();
	}

	public function u_detail_delivery_order($id, $data){
		$this->db->where('no_do', $id);
		$query = $this->db->update('detail_delivery_order', $data);
		return $query;
	}

	public function d_detail_delivery_order($id){
		$query = $this->db->delete('detail_delivery_order', array('no_do' => $id));
		return $query;
	}

	public function r_received_delivery_order(){
		return $this->db->query("SELECT * FROM delivery_order WHERE status=3")->result();
	}

	public function r_delivery_order_without_datatable(){
		return $this->db->get('delivery_order')->result();
	}

	public function r_delivery_order_join_user_rol_lvl_2_byId($id){
		$this->db->from('delivery_order');
		$this->db->join('user_role_level_2', 'user_role_level_2.id_user_role = delivery_order.id_role_receive');
		return $this->db->where('delivery_order.no_do', $id)->get()->row();
	}

	public function r_detail_delivery_order_by_gsOneId($id){
		return $this->db->get_where('detail_delivery_order', ['gs_on_id' => $id])->row();
	}
}
?>