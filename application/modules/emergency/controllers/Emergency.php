<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Emergency extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Emergency_model');
 	}

 	public function getEmergency()
 	{
 		$startDate = $this->input->post('startDate');
 		$endDate = $this->input->post('endDate');
		$list = $this->Emergency_model->r_emergency($startDate, $endDate);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->category;
			$row[] = $data->description;
			$row[] = $data->foto;
			$row[] = $data->new_no_pol;
			$row[] = $data->time_emergency;
			$row[] = $data->username;
			$row[] = '<a href="'.base_url('emergency/listDoEmergency/').$data->id_emergency.'" class="">
					   	<i class="las la-clipboard text-info font-18"></i>
					   </a> ';
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Emergency_model->r_emergency_count_all(),
                        "recordsFiltered" => $this->Emergency_model->r_emergency_count_filtered($startDate, $endDate),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function getReportEmergency()
 	{
 		$id = $this->input->post('id_emergency');
		$list = $this->Emergency_model->r_report_emergency($id);
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->no_do;
			$row[] = $data->eta;
			$row[] = $data->no_request;
			$row[] = $data->nm_role_lvl_2;
			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Emergency_model->r_report_emergency_count_all($id),
                        "recordsFiltered" => $this->Emergency_model->r_report_emergency_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function deleteEmergency($id)
 	{
 		$delete = $this->Emergency_model->d_emergency($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function getEmergencyById($id){
		echo json_encode($this->Emergency_model->r_emergency_by_id($id));
	}

	public function test(){
		echo json_encode($this->input->post('start'));
	}
	 
	// FOR VIEW
	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function listDoEmergency($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_do_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function detail($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('detail_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_emergency', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function report()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('report_emergency');
		$this->load->view('_rootComponents/_footer/footer');
	 }
}