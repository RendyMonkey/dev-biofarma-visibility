<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>

<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet"
    type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">Receive Order</a></li>
                                <li class="breadcrumb-item active">Detail</li>
                            </ol>
                        </div>
                        <h4 class="page-title"><?= $this->session->userdata('nm_role_lv_2');?></h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <h5 class="text-primary">Emergency Report</h5>

                            <div class="row">
                                <div class="col-6">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Kategori</th>
                                                <td id="kategori"></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Deksripsi</th>
                                                <td id="deskripsi"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Nomor Polisi</th>
                                                <td id="no_pol"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">Time Emergency</th>
                                                <td id="time_emergency"></td>
                                            </tr>

                                            <tr>
                                                <th scope="row">User account</th>
                                                <td id="user_account"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr class="text-left">
                                        <th>#</th>
                                        <th>No Delivery</th>
                                        <th>ETA</th>
                                        <th>No Request</th>
                                        <th>Instansi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
        <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
        <!--end footer-->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
  $.ajax({
    url: '<?= base_url('emergency/Emergency/getEmergencyById/').$id;?>',
    type: 'get',
    dataType: 'json',
    success: function(data) {
    document.getElementById("kategori").innerHTML = ": " + data.category;
    document.getElementById("deskripsi").innerHTML = ": " + data.description;
    document.getElementById("no_pol").innerHTML = ": " + data.new_no_pol;
    document.getElementById("time_emergency").innerHTML = ": " + data.time_emergency;
    document.getElementById("user_account").innerHTML = ": " + data.username;
    }
  });
       $('#datatable').DataTable({ 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?= site_url('emergency/Emergency/getReportEmergency/').$id;?>",
          "type": "POST",
          "data":{
              <?= $this->security->get_csrf_token_name(); ?> : '<?=$this->security->get_csrf_hash();?>',
              id_emergency : '<?=$id;?>'
          },
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
          },
        ],

      });
</script>