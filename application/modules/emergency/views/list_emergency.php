<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
        <!-- Page Content-->
        <div class="page-content-tab">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    
                                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Emergency</a></li>
                                    <li class="breadcrumb-item active">List Emergency</li>
                                </ol>
                            </div>
                            <h4 class="page-title">List Emergency</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div><!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="datatable" class="table table-bordered dt-responsive nowrap"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kategori Emergency</th>
                                            <th>Description</th>
                                            <th>Foto</th>
                                            <th>Nomor Polisi</th>
                                            <th>Time Emergency</th>
                                            <th>User Account</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: center;">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- container -->


            <!-- MODAL EDIT -->
            <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01">Menu Name</label>
                                <input type="text" class="form-control" id="validationCustom01" required>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Category</label>
                                <input type="text" class="form-control" id="validationCustom02" required>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                            </div>
                            
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button class="btn btn-primary" type="submit">Submit form</button>
                          </form>
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL DELETE -->
              <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      ...
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>

            <!--  Modal content for the above example -->
            <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking </footer>
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

    <script>                                        

       $('#datatable').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?= site_url('emergency/getEmergency')?>",
          "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
          },
        ],

      });


      function delete_data(id) {
        Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.isConfirmed) {
                $.ajax({
                  url: '<?= base_url('emergency/emergency/deleteEmergency/') ?>' + id,
                  type: 'get',
                  dataType: 'json',
                  success: function(data) {
                    $('#datatable').DataTable().ajax.reload(null, false);
                    Swal.fire(
                      'Deleted!',
                      'Your file has been deleted.',
                      'success'
                    );              
                  },
                  error: function () {
                    alert('gagal');
                  }
                });
              }
            })
      }
    </script>