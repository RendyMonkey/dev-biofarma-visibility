<?php 
class Role_model extends CI_Model{

	public function c_role($data){
		$query = $this->db->insert('role', $data);
		return $query;
	}

	public function r_role_query(){
		$search_attribut = ["kd_role", "nm_role"];
		$column_order = array(null, "kd_role", "nm_role");
		search_datatable("role", $search_attribut, $column_order);
	}

	public function r_role(){
		$this->r_role_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}


	public function r_role_count_all()
    {
        $this->db->from('role');
        return $this->db->count_all_results();
	}
	
	function r_role_count_filtered()
    {
        $this->r_role_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	public function r_role_all(){

		$query = $this->db->get('role');
		return $query->result();
	}

	public function u_role($id, $data){
		$this->db->where('id_role', $id);
		$query = $this->db->update('role', $data);
		return $query;
	}

	public function d_role($id){
		$query = $this->db->delete('role', array('id_role' => $id));
		return $query;
	}

	public function r_role_by_id($id){
		$query = $this->db->get_where('role', array('id_role' => $id));
		return $query->row();
	}

	public function r_role_for_where_not_byId($id){
		$this->db->from('role');
		return $this->db->where_not_in('id_role', $id)->get()->result();
	}

}
?>