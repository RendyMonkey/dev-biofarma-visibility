<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Role</a></li>
                                    <li class="breadcrumb-item active">Create Role Level 2</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Role Level 2</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                        <div class="form-group">
                                    <label for="category">Nama Instansi</label>
                                    <input type="text" class="form-control" name="nama_instansi" id="nama_instansi" value="<?=$data->nama_instansi;?>">
                                </div>
                        <div class="form-group">
                                    <label for="category">Role Level 2 Code</label>
                                    <input type="text" class="form-control" name="kd_role_lvl_2" id="kd_role_lvl_2" value="<?=$data->kd_role_lvl_2;?>">
                                </div>

                                <div class="form-group">
                                    <label for="category">Role Level 2 Name</label>
                                    <input type="text" class="form-control" name="nm_role_lvl_2" id="nm_role_lvl_2" value="<?=$data->nm_role_lvl_2;?>">
                                </div>
                                
                                <div class="form-group">
                                    <label for="category">Role</label>
                                    <select class="form-control" name="id_role" id="id_role">
                                    <option value="<?=$data->id_role;?>"><?=$data->nm_role;?></option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label for="category">Address</label>
                                    <input type="text" class="form-control" name="alamat_lengkap" id="alamat_lengkap" value="<?=$data->alamat_lengkap;?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Contact Name</label>
                                    <input type="text" class="form-control" name="contact_name" id=contact_name value="<?=$data->contact_name;?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Telephone</label>
                                    <input type="text" class="form-control" name="telepon" id="telepon" value="<?=$data->telepon;?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Fax</label>
                                    <input type="text" class="form-control" name="fax" id="fax" value="<?=$data->fax;?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" value="<?=$data->email;?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Location Werehouse</label>
                                    <input type="text" class="form-control" name="location_warehouse" id="location_warehouse" value="<?=$data->location_warehouse?>">
                                </div>
                                <div class="form-group">
                                    <label for="category">Parent User Role Level 2</label>
                                    <select class="form-control" id="id_role_parent">
                                        <option value="<?=$data->id_role_parent?>"><?=$data->nm_role_parent;?></option>
                                    </select>
                                </div>
                                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->
<h1><?=$data->id_user_role;?></h1>
<script>
    $.ajax({
            url: '<?= base_url('user/UserRoleLevel2/getAllUserRoleLevel2WhereNotById/'.$data->id_role_parent) ?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_user_role + '">' + dataValue.nm_role_lvl_2 + '</option>'
                    $("#id_role_parent").append(html);
                });
            }
        });

    $.ajax({
            url: '<?= site_url('role/getAllRoleWhereNotById/'.$data->id_role)?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_role + '">' + dataValue.nm_role + '</option>'
                    $("#id_role").append(html);
                });
            }
        });

    $('#submit').click( function(){
        if(($('#nama_instansi').val().length < 1) || ($('#contact_name').val().length < 1) || ($('#kd_role_lvl_2').val() < 1) || ($('#nm_role_lvl_2').val() < 1) || ($('#alamat_lengkap').val() < 1) || ($('#email').val() < 1) || ($('#id_role').val() == 'choose') ){
          if($('#nama_instansi').val().length < 1){
            document.getElementById('nama_instansi').focus();
            var message = "Nama instansi is required";
          }else if($('#contact_name').val().length < 1){
            document.getElementById('contact_name').focus();
            var message = "Contact name is required";
          }else if($('#kd_role_lvl_2').val().length < 1){
            document.getElementById('kd_role_lvl_2').focus();
            var message = "Kode role is required";
          }else if($('#nm_role_lvl_2').val().length < 1){
            document.getElementById('nm_role_lvl_2').focus();
            var message = "Nama role is required";
          }else if($('#alamat_lengkap').val().length < 1){
            document.getElementById('alamat_lengkap').focus();
            var message = "Alamat is required";
          }else if($('#id_role').val() == 'choose'){
            var message = "Role is required";
          }else{
            var message = "Email is required !";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );
    }else{
        $.ajax({
            url: '<?= base_url('user/UserRoleLevel2/updateUserRoleLevel2/').$data->id_user_role;?>',
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
                nama_instansi: $('#nama_instansi').val(),
                contact_name: $('#contact_name').val(),
                id_role_parent: $('#id_role_parent').val(),
                id_role: $('#id_role').val(),
                kd_role_lvl_2: $('#kd_role_lvl_2').val(),
                nm_role_lvl_2: $('#nm_role_lvl_2').val(),
                alamat_lengkap: $('#alamat_lengkap').val(),
                telepon: $('#telepon').val(),
                fax: $('#fax').val(),
                email: $('#email').val(),
                location_warehouse: $('#location_warehouse').val()
            },
            success: function (data) {
                console.log(data);
                Swal.fire(
                'Data has been updated!',
                'Your data is successfuly updated.',
                'success'
                );
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
    }
       });
</script>