<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap4.min.css') ?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<div class="page-wrapper">
        <!-- Page Content-->
        <div class="page-content-tab">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    
                                    <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Role</a></li>
                                    <li class="breadcrumb-item active">List Role Level 2</li>
                                </ol>
                            </div>
                            <h4 class="page-title">List Role Level 2</h4>
                        </div>
                        <!--end page-title-box-->
                    </div>
                    <!--end col-->
                </div><!-- end page title end breadcrumb -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="datatable" class="table table-bordered"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Role Code</th>
                                            <th>Role Name</th>
                                            <th>Address</th>
                                            <th>Telephone</th>
                                            <th>Fax</th>
                                            <th>Email</th>
                                            <th>Location Werehouse</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- container -->

            <footer class="footer text-center text-sm-left">&copy; 2020 Biotracking</footer>
            <!--end footer-->
        </div><!-- end page content -->
    </div><!-- end page-wrapper -->

    <script>
       var table =  $('#datatable').DataTable({

        "processing": true,
        "serverSide": true,
        "responsove": true,
        "order": [],

        "ajax": {
          "url": "<?= site_url('user/UserRoleLevel2/getUserRoleLevel2')?>",
          "type": "POST"
        },

      "columnDefs": [{
        "targets": [0],
        "orderable": false, 
      }, ],

        });

function delete_data(id) {
    Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.isConfirmed) {
            $.ajax({
              url: '<?= base_url('user/UserRoleLevel2/deleteUserRoleLevel2/') ?>' + id,
              type: 'get',
              dataType: 'json',
              success: function(data) {
                table.ajax.reload(null, false);
                Swal.fire(
                  'Deleted!',
                  'Your file has been deleted.',
                  'success'
                );              
              },
              error: function () {
                alert('gagal');
              }
            });
          }
        })
  }

    </script>