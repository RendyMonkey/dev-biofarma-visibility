<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Partisipan extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Partisipan_model');
 		$this->load->model('Url_model');
 	}

 	public function createPartisipan()
 	{
		$data = [
            'partner_name' => $this->input->post('partner_name'),
            'qoute' => $this->input->post('quote'),
			'token' => uniqid(10),
            'is_unlimited' => $this->input->post('is_unlimited'),
            'token_expired_date' => date("Y-m-d", mktime(0,0,0,date("n"),date("j")+90,date("Y"))),
            "created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
        ];
 		$input_data = $this->Partisipan_model->c_partisipan($data);
 		if($input_data){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getPartisipan()
	 { 
		$list = $this->Partisipan_model->r_partisipan();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->partner_name;
			$row[] = $data->quote;
			$row[] = $data->token;
			$row[] = '<a href="'.base_url('manajemen_open_api/Partisipan/update/').$data->partner_id.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a onclick="delete_data('.$data->partner_id.')" href="#" class="delete mr-2" data-id="'.$data->partner_id.'" >
					 <i class="las la-trash-alt text-danger font-18 del" data-id="'.$data->partner_id.'"></i>
												</a>
												</a>
												<a onclick="reganerate_token('.$data->partner_id.')" href="#" class="delete" data-id="'.$data->partner_id.'" >
												Regenerate Token
																		   </a>';

			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Partisipan_model->r_partisipan_count_all(),
                        "recordsFiltered" => $this->Partisipan_model->r_partisipan_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
     }

     public function getUrl()
	 { 
		$list = $this->Url_model->r_url();
		$datas = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $data->url;
			$row[] = $data->method;
			$datas[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Url_model->r_url_count_all(),
                        "recordsFiltered" => $this->Url_model->r_url_count_filtered(),
                        "data" => $datas,
		); 
 		echo json_encode($result);
     }
     
    public function getPartisipanlWithoutDatatable(){
        echo json_encode($this->Partisipan_model->r_partisipan_without_datatable());
    }

    public function getUrlWithoutDatatable(){
        echo json_encode($this->Url_model->r_url_without_datatable());
    }

 	public function deletePartisipan($id)
 	{
 		$delete = $this->Partisipan_model->d_partisipan($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function regenerateToken($id){
		$data = [
            'token' => uniqid(10),
            'expired_token' => date("Y-m-d", mktime(0,0,0,date("n"),date("j")+90,date("Y"))),
        ];
		$regenerate = $this->Partisipan_model->u_partisipan($id, $data);
 		echo json_encode(['message' => 'regenerate token berhasil']);
	} 

	 public function list()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_Partisipan_page');
		$this->load->view('_rootComponents/_footer/footer');
     }
     
     public function updatePartisipan($id){
         $data = [
             'kode_partisipan' => $this->input->post('kode_partisipan')
         ];
         $this->Partisipan_model->u_partisipan($id, $data);
         echo json_encode(['message' => 'update data berhasil']);
     }
	//  public function detail($id)
	//  {
	// 	$this->load->view('_rootComponents/_header/heder');
	// 	$this->load->view('_rootComponents/_sidebar/sidebar');
	// 	$this->load->view('_rootComponents/_navbar/navbar');
	// 	$this->load->view('detail_delivery', ['id' => $id]);
	// 	$this->load->view('_rootComponents/_footer/footer');
	//  }
 
	 public function create()
	 {
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_partisipan_page');
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function update($id)
	 {
        $data = $this->Partisipan_model->r_partisipan_by_id($id);
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_partisipan', ['partisipan' => $data]);
		$this->load->view('_rootComponents/_footer/footer');
	 }

	 public function getUrlWhereNotIn($id){
		 echo json_encode($this->Url_model->r_url_where_not_in($id));
	 }

	 public function getPartisipanWhereNotIn($id){
		echo json_encode($this->Partisipan_model->r_partisipan_where_not_in($id));
	 }
	 
}