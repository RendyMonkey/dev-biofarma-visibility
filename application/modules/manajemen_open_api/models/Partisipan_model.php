<?php 
class Partisipan_model extends CI_Model{

	public function c_partisipan($data){
		$query = $this->db->insert('partner', $data);
		return $query;
	}

	public function r_partisipan_query(){
		$search_attribut = ["partner_name", "quote", "token"];
		$column_order = array(null, "partner_name", "quote", "token");
		search_datatable("partner", $search_attribut, $column_order);
	}

	public function r_partisipan() 
	{
		$this->r_partisipan_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function r_partisipan_without_datatable()
    {
        return $this->db->get('partner')->result();
    }

    public function r_partisipan_by_id($id){
        return $this->db->get_where('partner', ['partner_id' => $id])->row();
    }

	function r_partisipan_count_filtered()
    {
        $this->r_partisipan_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function r_partisipan_count_all()
    {
        $this->db->from('partner');
        return $this->db->count_all_results();
    }

	public function u_partisipan($id, $data){
		$this->db->where('partner_id', $id);
		$query = $this->db->update('partner', $data);
		return $query;
	}
    
	public function d_partisipan($id){
		$query = $this->db->delete('partner', array('partner_id' => $id));
		return $query;
    }
    
    public function r_partisipan_where_not_in($id){
        $this->db->from('partner');
        return $this->db->where_not_in('partner', $id)->get()->result();
    }
}
?>