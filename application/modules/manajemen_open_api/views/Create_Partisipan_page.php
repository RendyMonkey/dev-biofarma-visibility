<link href="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
<script src="<?= base_url('assets/plugins/timepicker/bootstrap-material-datetimepicker.js') ?>"></script>
<link href="<?= base_url('assets/plugins/dropify/dropify.min.css') ?>" rel="stylesheet">
<script src="<?= base_url('assets/plugins/dropify/dropify.min.js') ?>"></script>

<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Partner</a></li>
                                    <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Partner</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class=" col-8 offset-2" enctype="multipart/form-data" id="frm">
                            <input type="text" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none;">
                                <div class="form-group">
                                    <label for="category">Partner Name</label>
                                    <input type="text" class="form-control" name="" id="partner_name">
                                </div>
                                <div class="form-group">
                                    <label for="validationCustom02">Is unlimited</label>
                                    <select class="form-control" id="is_unlimited" required>
                                        <option value="choose">Choose..</option>
                                        <option value='true'>True</option>
                                    <option value='false'>False</option>
                                </select>
                              </div>
                                <div class="form-group">
                                    <label for="category">Quote</label>
                                    <input type="text" class="form-control" name="" id="quote">
                                </div>
                                
                                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
    $(document).ready(function () {
        $("#submit").click(function () {

            $.ajax({
                url: '<?= base_url('manajemen_open_api/partisipan/createPartisipan') ?>',
                type: 'post',
                dataType: 'json',
                processData: false,                
                contentType: false,
                data: new FormData(frm),
                success: function (response) {
                    Swal.fire({
                        icon:   'success',
                        title:  'Success!',
                        html:   '<p class="h4">Your data is successfully input.</p>' 
                        + '<a class="card-lin" href="<?= base_url('delivery_order/DeliveryOrder/list') ?>">Go to list page!</a>'
                    });
                
                $("#partner_name").val("");
                $("#is_unlimited").val("choose");
                $("#quote").val("");

                },
                error: function () {
                    alert('gagal');
                }
            });
        });
    });

</script>