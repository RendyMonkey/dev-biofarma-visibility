<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Akses Api</a></li>
                                    <li class="breadcrumb-item active">Update</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Update Akses Api</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01">Url</label>
                                <select class="form-control" id="id_url" required>
                                <option value="<?=$data->id_url;?>"><?= $data->url;?></option>
                                </select>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Partisipan</label>
                                <select class="form-control" id="id_partisipan" required>
                                <option value="<?=$data->id_partisipan;?>"><?= $data->kode_partisipan;?></option>
                                </select>
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit form</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>

    $.ajax({
            url: '<?= site_url('manajemen_open_api/partisipan/getUrlWhereNotIn/'.$data->id_url.'/web')?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_url + '">' + dataValue.url + '</option>'
                    $("#url").append(html);
                });
            }
        });

        $.ajax({
            url: '<?= site_url('manajemen_open_api/partisipan/getPartisipanWhereNotIn/'.$data->id_partisipan)?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_partisipan + '">' + dataValue.kd_partisipan + '</option>'
                    $("#id_role").append(html);
                });
            }
        });

  $('#submit').click( function(){
        $.ajax({
            url: '<?= base_url('manajemen_open_api/AksesApi/updateAksesApi/'.$data->id_akses_api); ?>',
            type: 'post',
            dataType: 'json',
            data: {
              <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
              id_url: $('#id_url').val(),
              id_partisipan: $('#id_partisipan').val()
            },
            success: function (data) {
                Swal.fire(
                'Data has been returned!',
                'Your data is successfuly created.',
                'success'
                );
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
       });
</script>