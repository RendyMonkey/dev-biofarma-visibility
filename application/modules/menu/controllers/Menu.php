<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Menu extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->helper('url');
 		$this->load->model('Menu_model');
 	}

 	public function createMenu()
 	{
 		$data = [
 			"nama_menu" => $this->input->post('nama_menu'),
			"category" => $this->input->post('category'),
			"created_by" => $this->session->userdata('id_user_account'),
			"created_date" => date("Y-m-d H:i:s"),
		 ];
		 
 		$input = $this->Menu_model->c_menu($this->security->xss_clean($data));
 		if($input){
 			echo json_encode(['message' => 'input data berhasil']);
 		}else{
 			echo json_encode(['message' => 'input data gagal']);
 		}
 	}

 	public function getMenu()
 	{
		$list = $this->Menu_model->r_menu();
		$all_data = array();
		$no   = $_POST['start'];
		foreach($list as $data) {
			$row = array();
			$no++;
			$row[] = $no;
			$row[] = $data->nama_menu;
			$row[] = $data->category;
			$row[] = '<a href="'. base_url("menu/update/").$data->id_menu.'" class="mr-2">
					   	<i class="las la-pen text-info font-18"></i>
					   </a> 
                    <a href="#" onclick="delete_data('.$data->id_menu.')">
					 <i class="las la-trash-alt text-danger font-18"></i>
												</a>';

			$all_data[] = $row;
		}
		$result = array(
						"draw" => $_POST['draw'],
                        "recordsTotal" => $this->Menu_model->r_menu_count_all(),
                        "recordsFiltered" => $this->Menu_model->r_menu_count_filtered(),
                        "data" => $all_data,
		); 
 		echo json_encode($result);
 	}

 	public function updateMenu($id)
 	{
 		$data = [
 			"nama_menu" => $this->input->post('nama_menu'),
			"category" => $this->input->post('category'),
			"updated_by" => $this->session->userdata('id_user_account'),
			"updated_date" => date("Y-m-d H:i:s"), 
 		];
		
		$data = $this->security->xss_clean($data);
 		$update = $this->Menu_model->u_menu($id, $data);
 		if($update){
 			echo json_encode(['message' => 'update data berhasil']);
 		}else{
 			echo json_encode(['message' => 'update data gagal']);
 		}
	 }
	 
	public function getMenuById($id)
	{
		$result = $this->Menu_model->r_menu_by_id($id);
		echo json_encode($result);
	}

	public function getMenuAll()
	{
		$result = $this->Menu_model->r_menu_all();
		echo json_encode($result);
	}

 	public function deleteMenu($id)
 	{
 		$delete = $this->Menu_model->d_menu($id);
 		echo json_encode(['message' => 'delete data berhasil']);
	}

	public function getAllMenuByType($type){
		echo json_encode($this->Menu_model->r_menu_where_type($type));
	}
	
	public function list()
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('list_menu');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function create() 
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('create_menu');
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function update($id) 
	{
		$this->load->view('_rootComponents/_header/heder');
		$this->load->view('_rootComponents/_sidebar/sidebar');
		$this->load->view('_rootComponents/_navbar/navbar');
		$this->load->view('update_menu', ['id' => $id]);
		$this->load->view('_rootComponents/_footer/footer');
	}

	public function getAllMenuWhereNotById($id, $type){
		echo json_encode($this->Menu_model->r_menu_where_not_byId($id, $type));
	}
}