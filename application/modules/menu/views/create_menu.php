<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Menu</a></li>
                                    <li class="breadcrumb-item active">Create Menu</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Menu</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" id="form_menu" method="post">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01">Menu Name</label>
                                <input type="text" class="form-control" id="nama_menu" required>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Category</label>
                                <select class="form-control" id="category" required>
                                <option value="choose">Choose..</option>
                                <option value="web">Web</option>
                                <option value="mobile">Mobile</option>
                                </select>
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit</button>
                          </form>
                        </div>
                        <video id="webcam-preview"></video>
                        <p id="result"></p>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>
  
  $(document).ready(function() {
    $("#form_menu").validate();
  });
  
  $('#submit').click( function(){
        var category = document.getElementById('category');
        if(($('#nama_menu').val().length < 1) || (category.value == 'choose')){
          if($('#nama_menu').val().length < 1){
            document.getElementById('nama_menu').focus();
            var message = "Name menu is required";
          }else{
            var message = "Choose category !";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );

        }else{
          $.ajax({
            url: '<?= base_url('menu/menu/createMenu/') ?>',
            type: 'post',
            dataType: 'json',
            data: {
              <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
              nama_menu: $('#nama_menu').val(),
              category: category.value  
            },
            success: function (data) {
                Swal.fire(
                'Input Success',
                'Your data is successfuly returned.',
                'success'
                );
                
                $('#nama_menu').val('');
                $('#category').val('');
            }, error: function(){
                Swal.fire(
                'Input Failed',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })  
        }
        
       });
</script>