<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Menu</a></li>
                                    <li class="breadcrumb-item active">Update Menu</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Update Menu</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01">Menu Name</label>
                                <input type="text" class="form-control" id="nama_menu" required>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Category</label>
                                <input type="text" class="form-control" id="category" required>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

<script>

    $.ajax({
        url: '<?= base_url('menu/menu/getMenuById/').$id ?>',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            $("#nama_menu").val(data.nama_menu);
            $("#category").val(data.category);
        }
      });

  $('#submit').click( function(){
    if(($('#nama_menu').val().length < 1) || ($('#category').val().length < 1)){
          if($('#nama_menu').val().length < 1){
            document.getElementById('nama_menu').focus();
            var message = "Name menu is required";
          }else{
            document.getElementById('category').focus();
            var message = "Category is required";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          );
        }else{
            $.ajax({
            url: '<?= base_url('menu/menu/updateMenu/').$id ?>',
            type: 'post',
            dataType: 'json',
            data: {
              <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',
              nama_menu: $('#nama_menu').val(),
              category: $('#category').val()
            },
            success: function (data) {
                Swal.fire(
                'Data has been returned!',
                'Your data is successfuly returned.',
                'success'
                );
            }, error: function(){
                Swal.fire(
                'Data not returned!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
        }
        
       });
</script>