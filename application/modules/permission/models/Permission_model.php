<?php 
class Permission_model extends CI_Model{

	public function c_permission($type, $data){
		$query = $this->db->insert($type, $data);
		return $query;
	}

	public function r_permission($type){
		$query = $this->db->query("SELECT ".$type.".* , role.nm_role , menu.nama_menu FROM ".$type." JOIN role ON role.id_role = ".$type.".id_role JOIN menu ON ".$type.".id_menu = menu.id_menu");
		return $query->result();
	}

	public function r_permission_web_query(){
		$search_attribut = ["menu.nama_menu", "role.nm_role"];
		$column_order = array(null, "menu.nama_menu", "role.nm_role");
		$this->db->select('menu.nama_menu, permission_web.id_permission_web, role.nm_role');
		$this->db->from('menu');
		$this->db->join('permission_web', 'permission_web.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_web.id_role');
		$i = 0;
		foreach($search_attribut as $search) {
			if($_POST['search']['value'])
			{
				if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($search, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($search, $_POST['search']['value']);
                }

                if(count($search_attribut) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
			}			
            $i++;
		}

		if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function r_permission_mobile_query(){
		$search_attribut = ["menu.nama_menu", "role.nm_role"];
		$column_order = array(null, "menu.nama_menu", "role.nm_role");
		$this->db->select('menu.nama_menu, permission_mobile.id_permission_mobile, role.nm_role');
		$this->db->from('menu');
		$this->db->join('permission_mobile', 'permission_mobile.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_mobile.id_role');
		$i = 0;
		foreach($search_attribut as $search) {
			if($_POST['search']['value'])
			{
				if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($search, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($search, $_POST['search']['value']);
                }

                if(count($search_attribut) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
			}			
            $i++;
		}

		if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function r_permission_web(){
		$this->r_permission_web_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function r_permission_mobile(){
		$this->r_permission_mobile_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function u_permission($type, $id_permission, $data){
		$this->db->where(['id_'.$type => $id_permission]);
        $query = $this->db->update($type, $data);
		return $query;
	}

	public function d_permission($type, $id_permission){
		$query = $this->db->delete($type, array('id_'.$type => $id_permission));
		return $query;
	}

	public function r_permission_web_count_all()
    {
        $this->db->from('menu');
		$this->db->join('permission_web', 'permission_web.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_web.id_role');
        return $this->db->count_all_results();
	}
	
	
	public function r_permission_mobile_count_all()
    {
        $this->db->from('menu');
		$this->db->join('permission_mobile', 'permission_mobile.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_mobile.id_role');
        return $this->db->count_all_results();
	}

	function r_permission_web_count_filtered()
    {
        $this->r_permission_web_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_permission_mobile_count_filtered()
    {
        $this->r_permission_mobile_query();
        $query = $this->db->get();
        return $query->num_rows();
	}

	function r_permission_join_menu_role_by_id($id)
	{
		$this->db->select('menu.id_menu, menu.nama_menu, permission_web.id_permission_web, role.nm_role, role.id_role');
		$this->db->from('menu');
		$this->db->join('permission_web', 'permission_web.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_web.id_role');
		return $this->db->where('id_permission_web', $id)->get()->row();
	}

	function r_permission_mobile_join_menu_role_by_id($id)
	{
		$this->db->select('menu.id_menu, menu.nama_menu, permission_mobile.id_permission_mobile, role.nm_role, role.id_role');
		$this->db->from('menu');
		$this->db->join('permission_mobile', 'permission_mobile.id_menu = menu.id_menu');
		$this->db->join('role', 'role.id_role = permission_mobile.id_role');
		return $this->db->where('id_permission_mobile', $id)->get()->row();
	}

}
?>