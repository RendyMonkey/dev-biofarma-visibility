<div class="page-wrapper">
    <!-- Page Content-->
    <div class="page-content-tab">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javascript:void(0);">Permission</a></li>
                                    <li class="breadcrumb-item active">Create Permission Mobile</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Create Permission</h4>
                    </div>
                    <!--end page-title-box-->
                </div>
                <!--end col-->
            </div><!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="needs-validation" novalidate>
                            <div class="form-row">
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom01">Role</label>
                                <select class="form-control" id="id_role" required>
                                <option value="choose">Choose..</option>
                                </select>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                              <div class="col-md-6 mb-3">
                                <label for="validationCustom02">Menu</label>
                                <select class="form-control" id="id_menu" required>
                                <option value="choose">Choose..</option>
                                </select>
                                <div class="invalid-feedback">
                                  This field is required!
                                </div>
                              </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">Submit form</button>
                          </form>
                        </div>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div><!-- container -->
    </div><!-- end page content -->
</div><!-- end page-wrapper -->

    <script>
        
        $.ajax({
            url: '<?= site_url('menu/getAllMenuByType/Mobile')?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_menu + '">' + dataValue.nama_menu + '</option>'
                    $("#id_menu").append(html);
                });
            }
        });

        $.ajax({
            url: '<?= site_url('role/getRoleAll')?>',
            type: 'get',
            dataType: 'json',
            success: function(data){
                console.log(data);
                var html = '';
                $.each(data, function(key, dataValue){
                    html = '<option value="' + dataValue.id_role + '">' + dataValue.nm_role + '</option>'
                    $("#id_role").append(html);
                });
            }
        });

  $('#submit').click( function(){
        var role = document.getElementById('id_role');
        var menu = document.getElementById('id_menu');
        if((role.value == 'choose') || (menu.value == 'choose')){
          if(role.value == 'choose'){
            var message = "Role must be choose !";
          }else{
            var message = "Choose menu !";
          }
          Swal.fire(
              'Input Warning !',
              message,
              'warning'
          )
        }else{
        $.ajax({
            url: '<?= base_url('permission/permission/createPermission/permission_mobile') ?>',
            type: 'post',
            dataType: 'json',
            data: {
                <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>', 
              id_menu: $('#id_menu').val(),
              id_role: $('#id_role').val()
            },
            success: function (data) {
                Swal.fire(
                'Data has been returned!',
                'Your data is successfuly created.',
                'success'
                );
                
                $('#nama_menu').val('');
                $('#category').val('');
            }, error: function(){
                Swal.fire(
                'Data not created!',
                'Please contact developer to fix it.',
                'error'
                )
            }
        })
        }
       });
</script>