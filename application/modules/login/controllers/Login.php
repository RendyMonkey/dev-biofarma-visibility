<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

 	function __construct()
 	{
 		parent::__construct();
 		$this->load->library('session');
 		$this->load->helper('url');
 	}

 	public function index()
 	{
 		if($this->session->has_userdata('nama')){
 			redirect(base_url("home"));
 		}else{
			 $this->load->view('_components/_header/header');
			 $this->load->view('login_page');
			 $this->load->view('_components/_footer/footer');
 		}
 	}

	public function register() 
	{
		$this->load->view('_components/_header/header');
		$this->load->view('register_page');
		$this->load->view('_components/_footer/footer');
	}
	
	public function login_process()
	{
		$this->load->model('Login_model');
		$this->load->library('form_validation');
		$username = $this->input->post('username');
        $password = $this->input->post('password');
		$data['query'] = $this->Login_model->login($username, $password);
		if($data['query'] != null && password_verify($password, $data['query']->password)){
			$dataSession = ['id_user_account' => $data['query']->id_user_account, 'username'=> $data['query']->username, 'nama_role' => $data['query']->nm_role, 'nama'=> $data['query']->nm_lengkap, 'id_user_role_lv_2' => $data['query']->id_user_role, 'nm_role_lv_2' => $data['query']->nm_role_lvl2, 'statusLogin' => true];
			$this->session->set_userdata($dataSession);
			$response['message'] = "Login Berhasil";
			$response['access'] = true;
			$response['nama'] = $data['query']->nm_lengkap;
			echo json_encode($response);
		}else{
			$response['message'] = "Login Gagal";
			$response['access'] = false;
			echo json_encode($response);
		}
	}

	public function logout_process()
	{
		$dataSession = ['nama', 'statusLogin'];
		$this->session->unset_userdata($dataSession);
		$this->load->view('_components/_header/header');
		$this->load->view('login_page');
		$this->load->view('_components/_footer/footer');
	}

	public function test(){
		$data = [
			'email_penerima' => "praseptya123@gmail.com",
			'subject' => 'test',
			'body' => '<h1 style="color: red;">Test</h1>'
		];
		$this->load->library('mailer');
		$send = $this->mailer->send($data);
		echo "<b>".$send['status']."</b><br />";
    	echo $send['message'];
	}
}