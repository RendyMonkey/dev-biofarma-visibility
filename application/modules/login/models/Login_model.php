<?php 
class Login_model extends CI_Model{
	public function getData(){
		$query = $this->db->get('user_account');
		return $query->result();
	}

	public function login($username, $password){
		return $this->db->query("SELECT user_account.*, user_role_level_2.id_user_role, user_role_level_2.nm_role_lvl2, role.nm_role FROM user_account INNER JOIN user_role_level_2 ON user_account.id_role_level_2=user_role_level_2.id_user_role INNER JOIN role ON user_role_level_2.id_role=role.id_role WHERE user_account.username=".$this->db->escape($username))->row();
	}
}
?>