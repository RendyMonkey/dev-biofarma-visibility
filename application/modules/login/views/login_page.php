    <!-- Log In page -->
    <div class="container">
        <div class="row vh-100 d-flex justify-content-center">
            <div class="col-12 align-self-center">
                <div class="">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-5 mx-auto">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="media mb-3"><a href="../analytics/analytics-index.html"
                                                class="logo logo-admin"><img src="<?= base_url('/assets/images/logo-sm.png') ?>"
                                                    height="40" alt="logo" class="auth-logo"></a>
                                            <div class="media-body align-self-center text-truncate ml-2">
                                                <h4 class="mt-0 mb-1 font-weight-semibold text-dark font-18">Let's Get
                                                    Started Biotracking</h4>
                                                <p class="text-muted mb-0">Sign in to continue to Biotracking.</p>
                                            </div>
                                            <!--end media-body-->
                                        </div>
                                        <form class="form-horizontal auth-form my-4" action="index.html">
                                            <input type="hidden" class="txt_csrfname" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <div class="form-group"><label for="username">Username</label>
                                                <div class="input-group mb-3"><input type="text" class="form-control"
                                                        id="username" placeholder="Enter username"></div>
                                            </div>
                                            <!--end form-group-->
                                            <div class="form-group"><label for="userpassword">Password</label>
                                                <div class="input-group mb-3"><input type="password"
                                                        class="form-control" id="password"
                                                        placeholder="Enter password"></div>
                                            </div>
                                            <!--end form-group-->
                                           
                                            <div class="form-group mb-0 row">
                                                <div class="col-12 mt-2"><button
                                                        class="btn btn-primary btn-block waves-effect waves-light"
                                                        type="button" id="submit">Log In <i
                                                            class="fas fa-sign-in-alt ml-1"></i></button></div>
                                                <!--end col-->
                                            </div>

                                            <!-- <div class="m-3 text-center text-muted"><p>Don't have an account ? <a href="<?= base_url('login/login/register') ?>" class="text-primary ml-2">Resister</a></p></div> -->
                                            <!--end form-group-->
                                        </form>
                                        <!--end form-->
                                    </div>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->
            </div>
            <!--end col-->
        </div>
        <!--end row-->
    </div>
    <!--end container-->
    <!-- End Log In page -->
    <!-- jQuery  -->
