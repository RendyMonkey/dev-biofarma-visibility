<!-- End Log In page -->
<!-- jQuery  -->
<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery-ui.min.js') ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/js/metismenu.min.js') ?>"></script>
<script src="<?= base_url('assets/js/waves.js') ?>"></script>
<script src="<?= base_url('assets/js/feather.min.js') ?>"></script>
<script src="<?= base_url('assets/js/jquery.slimscroll.min.js') ?>"></script><!-- App js -->
<script src="<?= base_url('assets/js/app.js') ?>"></script>

<script type="text/javascript">
	    
	    $(document).ready(function(){ 
		    $("#submit").click(function(){
                var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
                var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		        var username = $("#username").val().trim();
		        var password = $("#password").val().trim();

		        if( username != "" && password != "" ){
		            $.ajax({
		                url:'<?php echo base_url('login/login_process') ?>',
		                type:'post',
		                dataType: 'json', 
		                data:{
                            [csrfName]: csrfHash,
                            username:username,
                            password:password},
		                success:function(response){
		                    if(response.access == true){
		                        window.location = "<?php echo base_url('home') ?>";
		                    }else{
                                Swal.fire(
                                        'Login Failed',
                                        'Username atau password gagal !',
                                        'error'
                                )   
		                    }
		                }
		            });
		        }else{
                    Swal.fire(
                        'Login Failed',
                        'Username atau password is required !',
                        'warning'
                    ) 
                }
		    });
		});
		
	</script>
</body>

</html>