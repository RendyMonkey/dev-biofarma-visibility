<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from mannatthemes.com/metrica/metrica_simple/ecommerce/ecommerce-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 13 Oct 2020 22:35:19 GMT -->

<head>
    <meta charset="utf-8">
    <title>Biotracking</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App favicon -->    
    <link rel="shortcut icon" href=<?= base_url('assets/images/logo-sm.png')?>
    <link href="<?= base_url('assets/plugins/lightpick/lightpick.css') ?>" rel="stylesheet"><!-- App css -->
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/jquery-ui.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/icons.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/metisMenu.min.css') ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/css/app.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery-ui.min.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.bundle.min.js')?>"></script>
    <link href="<?= base_url('assets/plugins/sweetAlert/sweetAlert.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= base_url('assets/plugins/sweetAlert/sweetAlert.js') ?>"></script>
</head>
<body>