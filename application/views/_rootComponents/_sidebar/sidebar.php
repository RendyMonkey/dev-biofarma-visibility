re<div class="leftbar-tab-menu">
    <div class="main-icon-menu"><a href="<?= base_url('home') ?>"
            class="logo logo-metrica d-block text-center"><span><img src="<?= base_url('assets/images/logo-sm.png') ?>"
                    alt="logo-small" class="logo-sm"></span></a>
        <nav class="nav">
        <?php if(has_permission_menu('Dashboard') > 0) { ?>
            <a href="#PageDashboard" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Dashboard" data-trigger="hover"><i data-feather="monitor"
                    class="align-self-center menu-icon icon-dual"></i> </a>
            <!--end MetricaEcommerce-->
        <?php } ?>

        <?php if(has_permission_menu('Menu') > 0) { ?>
            <a href="#PagePermission" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Permission" data-trigger="hover"><i data-feather="key"
                    class="align-self-center menu-icon icon-dual"></i> </a>
            <!--end MetricaUikit-->
        <?php } ?>
        
        <?php if(has_permission_menu('Emergency') > 0) { ?>
            <a href="#PageEmergency" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Emergency" data-trigger="hover"><i data-feather="alert-triangle"
                    class="align-self-center menu-icon icon-dual"></i> </a>
            <!--end MetricaPages-->
        <?php } ?>

        <?php if(has_permission_menu('Recooling') > 0) { ?>
            <a href="#PageRecolling" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Recolling" data-trigger="hover"><i data-feather="clipboard"
                    class="align-self-center menu-icon icon-dual"></i> </a>
            <!--end MetricaPages-->
        <?php } ?>

        <?php if(has_permission_menu('User') > 0) { ?>
            <a href="#PageUser" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Role" data-trigger="hover"><i data-feather="users"
                    class="align-self-center menu-icon icon-dual"></i> </a>
            <!--end MetricaApps-->
        <?php } ?>

        <?php if(has_permission_menu('Delivery Order') > 0) { ?>
            <a href="#DeliveryOrder" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Delivery Order" data-trigger="hover"><i data-feather="package"
                    class="align-self-center menu-icon icon-dual"></i></a>
            <!--end MetricaAuthentication-->
        <?php } ?>

        <?php if(has_permission_menu('Retur') > 0) { ?>
            <a href="#PageRetur" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Retur" data-trigger="hover"><i data-feather="corner-up-left"
                    class="align-self-center menu-icon icon-dual"></i></a>
            <!--end MetricaAuthentication-->
        <?php } ?>

            <!-- <a href="#Report" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Report" data-trigger="hover"><i data-feather="trending-up"
                    class="align-self-center menu-icon icon-dual"></i></a> -->
                    
        <?php if(has_permission_menu('Receive') > 0) { ?>
            <a href="#Receive" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Receive" data-trigger="hover"><i data-feather="box"
                    class="align-self-center menu-icon icon-dual"></i></a>
        <?php } ?>

        <?php if(has_permission_menu('Inventory') > 0) { ?>
        <a href="#Inventory" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Menu Inventory" data-trigger="hover"><i data-feather="book"
                    class="align-self-center menu-icon icon-dual"></i></a>
        </nav>
        <?php } ?>
        
        <?php if(has_permission_menu('Manajemen Api') > 0) { ?>
        <a href="#manajemenApi" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Manajemen Open API" data-trigger="hover"><i data-feather="code"
                    class="align-self-center menu-icon icon-dual"></i></a>
        </nav>
        <?php } ?>
        
        <a href="#reportData" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title=""
                data-original-title="Report data" data-trigger="hover"><i data-feather="file-text"
                    class="align-self-center menu-icon icon-dual"></i></a>
        </nav>
        
        <!--end nav-->
    </div>
    <!--end main-icon-menu-->
    <div class="main-menu-inner">
        <!-- LOGO -->
        <div class="topbar-left"><a href="<?= base_url('home'); ?>" class="logo">
                <h2>Biotracking</h2>
            </a>
        </div>
        <!--end logo-->
        <div class="menu-body slimscroll">
            <div id="PageDashboard" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Home</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('home'); ?>">Home</a></li>
                </ul>
            </div><!-- end Crypto -->

            <div id="PageUser" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Role</h6>
                </div>

                <!-- <ul class="nav metismenu">
                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">User Group</span><span
                                class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('/user/usergroup/list') ?>">List User Group</a>
                            </li>

                            <li>
                                <a href="<?= base_url('/user/usergroup/create') ?>">Create User Group</a>
                            </li>
                        </ul>
                    </li>
                </ul> -->

                <ul class="nav metismenu">
                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">User Role</span><span
                                class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('role/role/list') ?>">List User Role</a>
                            </li>

                            <li>
                                <a href="<?= base_url('role/role/create') ?>">Create User Role</a>
                            </li>
                        </ul>
                    </li>
                    <!--end nav-item-->
                </ul>
                <!--end nav-->


                <ul class="nav metismenu">
                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">User Role Level 2</span><span
                                class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="<?= base_url('/role/role/list_lvl_2') ?>">List Instansi/Warehouse</a>
                            </li>

                            <li>
                                <a href="<?= base_url('/role/role/create_lvl_2') ?>">Create Instansi/Warehouse</a>
                            </li>
                        </ul>
                    </li>
                    </ul>
                    <!--end nav-item-->

                    <ul class="nav metismenu">
                        <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">User Account</span><span
                                    class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="<?= base_url('user/useraccount/list') ?>">List User Account</a>
                                </li>

                                <li>
                                    <a href="<?= base_url('user/useraccount/create') ?>">Create User Account</a>
                                </li>
                            </ul>
                        </li>
                        </ul>
                        <!--end nav-item-->

            </div><!-- end Others -->

            <div id="PagePermission" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Permission</h6>
                </div>
                <ul class="nav metismenu">
                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">
                                Menu</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('menu/list') ?>">List
                                    Menu</a>
                            </li>

                            <li><a href="<?= base_url('menu/create') ?>">Create
                                    Menu</a>
                            </li>
                        </ul>
                    </li>
                    <!--end nav-item-->
                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">Permission
                                Web</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('permission/listPermissionWeb') ?>">List
                                    Permission Web</a>
                            </li>

                            <li><a href="<?= base_url('permission/createPermissionWeb') ?>">Create
                                    Permission Web</a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item"><a class="nav-link" href="#"><span class="w-100">Permission
                                Mobile</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="nav-second-level" aria-expanded="false">
                            <li><a href="<?= base_url('permission/listPermissionMobile') ?>">List
                                    Permission Mobile</a>
                            </li>

                            <li><a href="<?= base_url('permission/createPermissionMobile') ?>">Create
                                    Permission Mobile</a>
                            </li>
                        </ul>
                    </li>
                    <!--end nav-item-->
                </ul>
                <!--end nav-->

            </div><!-- end Others -->
            <div id="PageEmergency" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">List</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('emergency/list') ?>">Master
                            Emergency</a>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="<?= base_url('emergency/create') ?>">Create
                            Emergency</a>
                    </li> -->
                </ul>
            </div><!-- end Pages -->

            <div id="PageRecolling" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">List</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('recooling/list') ?>">Master
                            Recolling</a>
                    </li>

                    <!-- <li class="nav-item"><a class="nav-link" href="<?= base_url('recooling/create') ?>">Create
                            Recolling</a>
                    </li> -->
                </ul>
            </div><!-- end Pages -->

            <div id="DeliveryOrder" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">List Data</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('delivery_order/DeliveryOrder/list_pending') ?>">Pending Order</a>
                    </li>
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('delivery_order/DeliveryOrder/list') ?>">Delivery
                            Order</a>
                    </li>
                <div class="title-box">
                    <h6 class="menu-title">Create Data</h6>
                </div>
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('delivery_order/DeliveryOrder/create') ?>">Create
                            Delivery</a>
                    </li>
                </ul>
            </div><!-- end Pages -->


            <div id="PageRetur" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">List Data</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('retur/Retur/list') ?>">List Retur</a>
                    </li>
                <div class="title-box">
                    <h6 class="menu-title">Create Data</h6>
                </div>
                    <li class="nav-item"><a class="nav-link" href="<?= base_url('retur/Retur/create') ?>">Create
                            Retur</a>
                    </li>
                </ul>
            </div><!-- end Pages -->


            <div id="Report" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Report</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="#">Report Delevery Order</a>
                    </li>

                    <li class="nav-item"><a class="nav-link" href="#">Report
                            Retur</a>
                    </li>

                    <li class="nav-item"><a class="nav-link" href="#">Report Vaccene Usage</a>
                    </li>
                </ul>
            </div><!-- end Pages -->

            <div id="Receive" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">List Data</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('receive_order/ReceiveOrder/list') ?>">Receive
                            Order</a>
                    </li>
                </ul>
            </div><!-- end Pages -->
            <div id="Inventory" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Menu Inventory</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('Inventory/list') ?>">Inventory
                            Order</a>
                    </li>
                </ul>
            </div><!-- end Pages -->
            <div id="manajemenApi" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Partisipan</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('manajemen_open_api/Partisipan/list')?>">List Partisipan</a>
                    </li>
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('manajemen_open_api/Partisipan/create')?>">Create Partisipan</a>
                    </li>
                </ul>
                <div class="title-box">
                    <h6 class="menu-title">Url</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('manajemen_open_api/Url/list')?>">List URL</a>
                    </li>
                </ul>
                <div class="title-box">
                    <h6 class="menu-title">Akses API</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('manajemen_open_api/AksesApi/list')?>">List Akses</a>
                    </li>
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('manajemen_open_api/AksesApi/create')?>">Create Akses</a>
                    </li>
                </ul>
                
            </div><!-- end Pages -->
            <div id="reportData" class="main-icon-menu-pane">
                <div class="title-box">
                    <h6 class="menu-title">Report</h6>
                </div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link"
                            href="<?= base_url('emergency/report')?>">Report Emergency</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('delivery_order/DeliveryOrder/trackBackByGsOne')?>">
                            Track Back By GsOneId
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url('delivery_order/DeliveryOrder/trackBackByNoDo')?>">
                            Track Back By Delivery Order
                        </a>
                    </li>
                </ul>
                
            </div><!-- end Pages -->
        </div>
        <!--end menu-body-->
    </div><!-- end main-menu-inner-->
</div><!-- end leftbar-tab-menu-->